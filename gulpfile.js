var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    rename = require('gulp-rename'),
    browserify = require('gulp-browserify'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    plumber = require('gulp-plumber'),
	cleanCSS = require('gulp-clean-css'),
    notify = require('gulp-notify'),
	sourcemaps = require('gulp-sourcemaps');

function swallow(err) {
  console.log(err);
  notify.onError({
    title: "Gulp",
    subtitle: "Failure!",
    message: "Error <%= error.message %>",
    sound: "Beep"
  })(err);
  this.emit('end');
}

var paths = {
  "src": {
    "sass": "wp-content/themes/amin/src/scss/bundle.scss",
    "js": {
      "vendor": "wp-content/themes/amin/src/js/vendor/*.js",
      "main": "wp-content/themes/amin/src/js/main.js"
    }
  },
  "dest": {
    "sass": "wp-content/themes/amin/assets/css/",
    "js": "wp-content/themes/amin/assets/js/"
  },
  "watch": {
    "sass": "wp-content/themes/amin/src/scss/**/*.scss",
    "js": "wp-content/themes/amin/src/js/**/*.js"
  }
}

gulp.task('styles', function(){
  return gulp.src(paths.src.sass)
  .pipe(sourcemaps.init())
  .pipe(plumber({ errorHandler: swallow}))
  .pipe(sass({
    style: 'expanded'
  }))
  .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 9', 'ios 6', 'android 4'))
  .pipe(sourcemaps.write())
  .pipe(rename("style.css"))
  .pipe(gulp.dest(paths.dest.sass))
  //.pipe(cleanCSS({compatibility: 'ie8'}))
  .pipe(rename("style.min.css"))
  .pipe(gulp.dest(paths.dest.sass))
  .pipe(notify({
    title: 'Gulp',
    subtitle: "Styles Successfully Compiled",
    message: "The styles compiled successfully, you're the best at writing sass!"
  }))
});

gulp.task('scripts', function(){
  return gulp.src(paths.src.js.main)
  .pipe(plumber({ errorHandler: swallow}))
  // .pipe(sourcemaps.init())
  .pipe(browserify())
  .pipe(concat('bundle.js'))
  .pipe(gulp.dest(paths.dest.js))
  .pipe(uglify())
  .pipe(rename({suffix: '.min'}))
  // .pipe(sourcemaps.write())
  .pipe(gulp.dest(paths.dest.js));
});

gulp.task('vendor', function() {
  return gulp.src(paths.src.js.vendor)
  .pipe(plumber({ errorHandler: swallow}))
  .pipe(concat('vendor.js'))
  .pipe(uglify())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest(paths.dest.js));
});

gulp.task('watch', function() {
  gulp.watch(paths.watch.sass, ['styles']);
  gulp.watch(paths.watch.js, ['scripts', 'vendor']);
})
