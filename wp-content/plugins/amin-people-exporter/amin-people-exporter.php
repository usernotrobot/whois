<?php
/*
Plugin Name: AMIN People CSV Exporter
Description: Export all people in the post type person under their posiiton group
Plugin URI:
Author: Thinking Juice
Version: 0.1.0
Author URI: http://thinkingjuice.co.uk
*/

require_once( plugin_dir_path( __FILE__ ) . '/Exporter.php' );

function exporterInit() {
    $exporter = new Exporter();
    //$exporter->init();
}

function render_view() {
	//upload
	$html = "<h1>Thinking Juice CSV Person Exporter v0.1.0</h1>";
    $html .= "<p>Hit the button below to download a list of all people in the AMIN network.</p>";
	$html .= "<form action=\"\" method=\"POST\">";
	$html .= "<input class=\"button button-primary button-large\" type=\"submit\" name=\"download_csv\" value=\"Download CSV\">";
	$html .= "</form>";
	$html .= "<br>";
	echo $html;
}

function add_pages() {
	add_menu_page('People CSV Exporter' , 'People CSV Exporter' , 'edit_posts', 'csv-exporter.php', 'render_view');
    $exporter = new Exporter();
    $exporter->handle_form();
}

add_action('admin_menu', 'add_pages');

//add_action('wp_loaded', 'add_pages');
