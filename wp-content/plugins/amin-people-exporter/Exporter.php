<?php

class Exporter {
    protected $file;
    public $file_dir;
    protected $file_loc;

    function __construct() {
        $this->file = 'people.csv';
        $this->file_loc = fopen(plugin_dir_path( __FILE__ ) . '/people.csv', 'w');
        $this->file_dir = plugin_dir_path( __FILE__ ) . $this->file;
    }

    public function fetchPeople() {
        $args = array(
            'post_type' => 'person',
            'post_status' => 'publish',
            'posts_per_page' => -1
        );

        $query = new WP_Query($args);
        $posts = $query->posts;

        foreach($posts as $post) {
            $post->position_group = wp_get_post_terms($post->ID, 'position-group')[0]->name;
            $post->agency = get_field('agency', $post->ID)->post_title;
            $post->position = get_field('position', $post->ID);
            $post->email = get_field('email', $post->ID);
            $post->telephone = get_field('telephone', $post->ID);
            $post->facebook = get_field('facebook', $post->ID);
            $post->twitter = get_field('twitter', $post->ID);
            $post->linkedin = get_field('linkedin', $post->ID);
            $post->instagram = get_field('instagram', $post->ID);
        }

        return $posts;
    }

    /**
     * Prepares the data in a format ready for writing into the CSV
     * @param  Array $posts         Array of people posts
     * @return Array $formatted     Returns an array of data formatted ready to be written to the CSV
     */
    public function prepareData($posts) {
        $postsArray = json_decode(json_encode($posts), true); // I want these to be arrays. quick and dirty json decode and encode to convert from object to array.
        $grouped = $this->array_group_by($postsArray, 'position_group');

        $formatted = array(
            ['Person Name','Position Group','Position','Agency', 'Email', 'Telephone', 'Linkedin', 'Twitter', 'Facebook', 'Instagram']
        );

        foreach ($grouped as $group) {
            foreach ($group as $post) {
                array_push($formatted, array($post['post_title'], $post['position_group'], $post['position'], $post['agency'], $post['email'], $post['telephone'], $post['linkedin'], $post['twitter'], $post['facebook'], $post['instagram']));
            }
        }

        return $formatted;
    }

    public function exportPeople() {
        $data = $this->prepareData($this->fetchPeople());

        foreach ($data as $row) {
            fputcsv($this->file_loc, $row);
        }
    }

    public function download_csv() {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $this->file);
        readFile(plugin_dir_path( __FILE__ ) . $this->file);
        exit();
    }

    /**
     * Groups an array by a given key. Any additional keys will be used for grouping
     * the next set of sub-arrays.
     *
     * @author Jake Zatecky
     *
     * @param array $arr The array to have grouping performed on.
     * @param mixed $key The key to group or split by.
     *
     * @return array
     */
    public function array_group_by($arr, $key) {
    	if (!is_array($arr)) {
    		trigger_error('array_group_by(): The first argument should be an array', E_USER_ERROR);
    	}
    	if (!is_string($key) && !is_int($key) && !is_float($key)) {
    		trigger_error('array_group_by(): The key should be a string or an integer', E_USER_ERROR);
    	}
    	// Load the new array, splitting by the target key
    	$grouped = [];
    	foreach ($arr as $value) {
    		$grouped[$value[$key]][] = $value;
    	}
    	// Recursively build a nested grouping if more parameters are supplied
    	// Each grouped array value is grouped according to the next sequential key
    	if (func_num_args() > 2) {
    		$args = func_get_args();
    		foreach ($grouped as $key => $value) {
    			$parms = array_merge([$value], array_slice($args, 2, func_num_args()));
    			$grouped[$key] = call_user_func_array('array_group_by', $parms);
    		}
    	}
    	return $grouped;
    }

    public function handle_form() {
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
			if(!empty($_POST['download_csv'])) { //user wants to download all the peeps
                $this->exportPeople();
                $this->download_csv();
			}
		}
    }
}
