<?php

add_filter('get_twig', 'add_to_twig');

function add_to_twig($twig) {
	$twig->addFilter('share', new Twig_Filter_Function('share_link'));
	$twig->addFilter('wordslice', new Twig_Filter_Function('slice_words'));
	$twig->addFilter('countwords', new Twig_Filter_Function('count_words'));
	$twig->addFilter('extractsvg', new Twig_Filter_Function('extractSVG'));
    $twig->addFilter('checkURL', new Twig_Filter_Function('checkURL'));
    $twig->addFilter('extractSRC', new Twig_Filter_Function('extractSrc'));
    $twig->addFilter('alloha', new Twig_Filter_Function('alloha'));
    $twig->addFunction(new \Twig_SimpleFunction ('is_user_logged_in', function(  ) { return is_user_logged_in();} ));
    $twig->addFunction(new \Twig_SimpleFunction ('wp_logout_url', function(  ) { return wp_logout_url();} ));
	return $twig;
}

function share_link($post, $platform) {
	$url = '';
	switch($platform) {
		case 'facebook':
			$url = 'http://www.facebook.com/sharer.php?u=' . urlencode($post->permalink) . '&#038;t=' . urlencode($post->title);
			return $url;
			break;
		case 'twitter':
			$url = 'http://twitter.com/share?url=' . urlencode($post->permalink) . '&#038;text=' . urlencode($post->title);
			return $url;
			break;
		case 'linkedin':
			$url = 'http://www.linkedin.com/shareArticle?mini=true&#038;url=' . urlencode($post->permalink) . '&#038;title=' . urlencode($post->title);
			return $url;
			break;
		case 'google':
			$url = 'https://plus.google.com/share?url=' . urlencode($post->permalink);
			return $url;
			break;
		default:
			$url = $post->permalink;
			return $url;
	}
}

function slice_words($string, $offset, $length) {
	$words = explode(' ', $string);
	$slice = array_slice($words, $offset, $length);
	return implode(' ', $slice);
}

function count_words($string) {
	$words = explode(' ', $string);
	return count($words);
}

function checkURL($string) {
    if (strpos($string, 'http') === false) {
        return 'http://' . $string;
    } else {
        return $string;
    }
}

function extractSrc($html) {

    preg_match('|<iframe [^>]*(src="[^"]+")[^>]*|', $html, $matches);

    if (!empty($matches)) {
        return trim($matches[1]);
    } else {
        return $html;
    }
}

// function extractSVGFromString($string) {
// 	if ($file) {
//         return file_get_contents($file);
//     } else {
//         return;
//     }
// }
