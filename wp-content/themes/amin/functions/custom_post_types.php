<?php
/**
 * Define custom post types
 *
 * @since TJ Blank 1.0
 * @return void
 */

function change_post_object() {
  $labels = array(
    'name'               => _x( 'News', 'post type general name', 'your-plugin-textdomain' ),
    'singular_name'      => _x( 'News', 'post type singular name', 'your-plugin-textdomain' ),
    'menu_name'          => _x( 'News', 'admin menu', 'your-plugin-textdomain' ),
    'name_admin_bar'     => _x( 'News', 'add new on admin bar', 'your-plugin-textdomain' ),
    'add_new'            => _x( 'Add New', 'News', 'your-plugin-textdomain' ),
    'add_new_item'       => __( 'Add New News', 'your-plugin-textdomain' ),
    'new_item'           => __( 'New News', 'your-plugin-textdomain' ),
    'edit_item'          => __( 'Edit News', 'your-plugin-textdomain' ),
    'view_item'          => __( 'View News', 'your-plugin-textdomain' ),
    'all_items'          => __( 'All News', 'your-plugin-textdomain' ),
    'search_items'       => __( 'Search News', 'your-plugin-textdomain' ),
    'parent_item_colon'  => __( 'Parent News:', 'your-plugin-textdomain' ),
    'not_found'          => __( 'No News found.', 'your-plugin-textdomain' ),
    'not_found_in_trash' => __( 'No News found in Trash.', 'your-plugin-textdomain' )
  );

  $args = array(
    'labels'             => $labels,
    'public'             => true,
    'has_archive'        => false,
    'hierarchical'       => true,
    'rewrite'            => array( 'slug' => 'news' ),
    'show_ui'            => true,
    'show_in_menu'       => true
  );
  register_post_type('post', $args);
}

add_action( 'init', 'change_post_object' );

 function custom_post_type() {

  $labels = array(
       'name'               => _x( 'Agencies', 'post type general name', 'your-plugin-textdomain' ),
       'singular_name'      => _x( 'Agency', 'post type singular name', 'your-plugin-textdomain' ),
       'menu_name'          => _x( 'Agency Profile', 'admin menu', 'your-plugin-textdomain' ),
       'name_admin_bar'     => _x( 'Agencies', 'add new on admin bar', 'your-plugin-textdomain' ),
       'add_new'            => _x( 'Add New', 'Agency', 'your-plugin-textdomain' ),
       'add_new_item'       => __( 'Add New Agency', 'your-plugin-textdomain' ),
       'new_item'           => __( 'New Agency', 'your-plugin-textdomain' ),
       'edit_item'          => __( 'Edit Agency', 'your-plugin-textdomain' ),
       'view_item'          => __( 'View Agency', 'your-plugin-textdomain' ),
       'all_items'          => __( 'All Agencies', 'your-plugin-textdomain' ),
       'search_items'       => __( 'Search Agencies', 'your-plugin-textdomain' ),
       'parent_item_colon'  => __( 'Parent Agency:', 'your-plugin-textdomain' ),
       'not_found'          => __( 'No Agencies found.', 'your-plugin-textdomain' ),
       'not_found_in_trash' => __( 'No Agencies found in Trash.', 'your-plugin-textdomain' )
   );

   $args = array(
       'labels'             => $labels,
       'public'             => true,
       'publicly_queryable' => true,
       'show_ui'            => true,
       'show_in_menu'       => true,
       'query_var'          => true,
       'rewrite'            => array( 'slug' => 'agency' ),
       'capability_type'    => 'post',
       'menu_position'      => 1,
       'menu_icon' => 'dashicons-lightbulb',
       'map_meta_cap'       => true,
       'has_archive'        => false,
       'hierarchical'       => false,
       'menu_position'      => null,
       'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
   );

   register_post_type('agency', $args);
	 
	 

   $labels = array(
       'name'               => _x( 'Legacy Work', 'post type general name', 'your-plugin-textdomain' ),
       'singular_name'      => _x( 'Legacy Work', 'post type singular name', 'your-plugin-textdomain' ),
       'menu_name'          => _x( 'Legacy Work', 'admin menu', 'your-plugin-textdomain' ),
       'name_admin_bar'     => _x( 'Legacy Work', 'add new on admin bar', 'your-plugin-textdomain' ),
       'add_new'            => _x( 'Add New', 'Legacy Work', 'your-plugin-textdomain' ),
       'add_new_item'       => __( 'Add New Legacy Work', 'your-plugin-textdomain' ),
       'new_item'           => __( 'New Legacy Work', 'your-plugin-textdomain' ),
       'edit_item'          => __( 'Edit Legacy Work', 'your-plugin-textdomain' ),
       'view_item'          => __( 'View Legacy Work', 'your-plugin-textdomain' ),
       'all_items'          => __( 'All Legacy Work', 'your-plugin-textdomain' ),
       'search_items'       => __( 'Search Legacy Work', 'your-plugin-textdomain' ),
       'parent_item_colon'  => __( 'Parent Legacy Work:', 'your-plugin-textdomain' ),
       'not_found'          => __( 'No Legacy Work found.', 'your-plugin-textdomain' ),
       'not_found_in_trash' => __( 'No Legacy Work found in Trash.', 'your-plugin-textdomain' )
   );

   $args = array(
       'labels'             => $labels,
       'public'             => true,
       'publicly_queryable' => true,
       'show_ui'            => true,
		'show_in_menu' => 'edit.php?post_type=work',
       'query_var'          => true,
       'rewrite'            => array( 'slug' => 'work' ),
       'capability_type'    => 'post',
       'menu_icon' => 'dashicons-awards',
       'map_meta_cap'       => true,
       'has_archive'        => false,
       'hierarchical'       => false,
       'menu_position'      => null,
       'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
	   'show_ui' => true,
		'show_in_nav_menus' => false,
		'show_in_admin_bar' => false,
   );

   register_post_type('legacy-work', $args);
	 
	 
	 

   $labels = array(
       'name'               => _x( 'Work', 'post type general name', 'your-plugin-textdomain' ),
       'singular_name'      => _x( 'Work', 'post type singular name', 'your-plugin-textdomain' ),
       'menu_name'          => _x( 'Work', 'admin menu', 'your-plugin-textdomain' ),
       'name_admin_bar'     => _x( 'Work', 'add new on admin bar', 'your-plugin-textdomain' ),
       'add_new'            => _x( 'Add New', 'Work', 'your-plugin-textdomain' ),
       'add_new_item'       => __( 'Add New Work', 'your-plugin-textdomain' ),
       'new_item'           => __( 'New Work', 'your-plugin-textdomain' ),
       'edit_item'          => __( 'Edit Work', 'your-plugin-textdomain' ),
       'view_item'          => __( 'View Work', 'your-plugin-textdomain' ),
       'all_items'          => __( 'All Work', 'your-plugin-textdomain' ),
       'search_items'       => __( 'Search Work', 'your-plugin-textdomain' ),
       'parent_item_colon'  => __( 'Parent Work:', 'your-plugin-textdomain' ),
       'not_found'          => __( 'No Work found.', 'your-plugin-textdomain' ),
       'not_found_in_trash' => __( 'No Work found in Trash.', 'your-plugin-textdomain' )
   );

   $args = array(
       'labels'             => $labels,
       'public'             => true,
       'publicly_queryable' => true,
       'show_ui'            => true,
       'show_in_menu'       => true,
       'query_var'          => true,
       'rewrite'            => array( 'slug' => 'Work' ),
       'capability_type'    => 'post',
       'menu_icon' => 'dashicons-awards',
       'map_meta_cap'       => true,
       'has_archive'        => false,
       'hierarchical'       => false,
       'menu_position'      => null,
       'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
   );

   register_post_type('Work', $args);
	
	 
   $labels = array(
       'name'               => _x( 'Event', 'post type general name', 'your-plugin-textdomain' ),
       'singular_name'      => _x( 'Event', 'post type singular name', 'your-plugin-textdomain' ),
       'menu_name'          => _x( 'Events', 'admin menu', 'your-plugin-textdomain' ),
       'name_admin_bar'     => _x( 'Event', 'add new on admin bar', 'your-plugin-textdomain' ),
       'add_new'            => _x( 'Add New', 'Event', 'your-plugin-textdomain' ),
       'add_new_item'       => __( 'Add New Event', 'your-plugin-textdomain' ),
       'new_item'           => __( 'New Event', 'your-plugin-textdomain' ),
       'edit_item'          => __( 'Edit Event', 'your-plugin-textdomain' ),
       'view_item'          => __( 'View Event', 'your-plugin-textdomain' ),
       'all_items'          => __( 'All Event', 'your-plugin-textdomain' ),
       'search_items'       => __( 'Search Event', 'your-plugin-textdomain' ),
       'parent_item_colon'  => __( 'Parent Event:', 'your-plugin-textdomain' ),
       'not_found'          => __( 'No Event found.', 'your-plugin-textdomain' ),
       'not_found_in_trash' => __( 'No Event found in Trash.', 'your-plugin-textdomain' )
   );

   $args = array(
       'labels'             => $labels,
       'public'             => true,
       'publicly_queryable' => true,
       'show_ui'            => true,
       'show_in_menu'       => true,
       'query_var'          => true,
  	   'rewrite'            => array('slug' => 'events'),
       'capability_type'    => 'post',
       'menu_icon' => 'dashicons-nametag',
       'map_meta_cap'       => true,
       'has_archive'        => false,
       'hierarchical'       => false,
       'menu_position'      => null,
       'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
   );

   register_post_type('event', $args);

   $labels = array(
       'name'               => _x( 'Client', 'post type general name', 'your-plugin-textdomain' ),
       'singular_name'      => _x( 'Client', 'post type singular name', 'your-plugin-textdomain' ),
       'menu_name'          => _x( 'Clients', 'admin menu', 'your-plugin-textdomain' ),
       'name_admin_bar'     => _x( 'Client', 'add new on admin bar', 'your-plugin-textdomain' ),
       'add_new'            => _x( 'Add New', 'Client', 'your-plugin-textdomain' ),
       'add_new_item'       => __( 'Add New Client', 'your-plugin-textdomain' ),
       'new_item'           => __( 'New Client', 'your-plugin-textdomain' ),
       'edit_item'          => __( 'Edit Client', 'your-plugin-textdomain' ),
       'view_item'          => __( 'View Client', 'your-plugin-textdomain' ),
       'all_items'          => __( 'All Clients', 'your-plugin-textdomain' ),
       'search_items'       => __( 'Search Clients', 'your-plugin-textdomain' ),
       'parent_item_colon'  => __( 'Parent Clients:', 'your-plugin-textdomain' ),
       'not_found'          => __( 'No Clients found.', 'your-plugin-textdomain' ),
       'not_found_in_trash' => __( 'No Clients found in Trash.', 'your-plugin-textdomain' )
   );

   $args = array(
       'labels'             => $labels,
       'public'             => true,
       'publicly_queryable' => true,
       'show_ui'            => true,
       'show_in_menu'       => true,
       'query_var'          => true,
       'capability_type'    => 'post',
       'menu_icon' => 'dashicons-format-chat',
       'map_meta_cap'       => true,
       'has_archive'        => false,
       'hierarchical'       => false,
       'menu_position'      => null,
       'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
   );

   register_post_type('client', $args);

   $labels = array(
       'name'               => _x( 'Person', 'post type general name', 'your-plugin-textdomain' ),
       'singular_name'      => _x( 'Person', 'post type singular name', 'your-plugin-textdomain' ),
       'menu_name'          => _x( 'People', 'admin menu', 'your-plugin-textdomain' ),
       'name_admin_bar'     => _x( 'Person', 'add new on admin bar', 'your-plugin-textdomain' ),
       'add_new'            => _x( 'Add New', 'Person', 'your-plugin-textdomain' ),
       'add_new_item'       => __( 'Add New Person', 'your-plugin-textdomain' ),
       'new_item'           => __( 'New Person', 'your-plugin-textdomain' ),
       'edit_item'          => __( 'Edit Person', 'your-plugin-textdomain' ),
       'view_item'          => __( 'View Person', 'your-plugin-textdomain' ),
       'all_items'          => __( 'All People', 'your-plugin-textdomain' ),
       'search_items'       => __( 'Search People', 'your-plugin-textdomain' ),
       'parent_item_colon'  => __( 'Parent People:', 'your-plugin-textdomain' ),
       'not_found'          => __( 'No People found.', 'your-plugin-textdomain' ),
       'not_found_in_trash' => __( 'No People found in Trash.', 'your-plugin-textdomain' )
   );

   $args = array(
       'labels'             => $labels,
       'public'             => true,
       'publicly_queryable' => true,
       'show_ui'            => true,
       'show_in_menu'       => true,
       'query_var'          => true,
       'capability_type'    => 'post',
       'menu_icon' => 'dashicons-universal-access',
       'map_meta_cap'       => true,
       'has_archive'        => false,
       'hierarchical'       => false,
       'menu_position'      => null,
       'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt')
   );

   register_post_type('person', $args);

   $labels = array(
       'name'               => _x( 'Press News', 'post type general name', 'your-plugin-textdomain' ),
       'singular_name'      => _x( 'Press News Item', 'post type singular name', 'your-plugin-textdomain' ),
       'menu_name'          => _x( 'Press News', 'admin menu', 'your-plugin-textdomain' ),
       'name_admin_bar'     => _x( 'Press News', 'add new on admin bar', 'your-plugin-textdomain' ),
       'add_new'            => _x( 'Add Press News Item', 'Person', 'your-plugin-textdomain' ),
       'add_new_item'       => __( 'Add New Press News Item', 'your-plugin-textdomain' ),
       'new_item'           => __( 'New Press News Item', 'your-plugin-textdomain' ),
       'edit_item'          => __( 'Edit Press News Item', 'your-plugin-textdomain' ),
       'view_item'          => __( 'View Press News Item', 'your-plugin-textdomain' ),
       'all_items'          => __( 'All Press News', 'your-plugin-textdomain' ),
       'search_items'       => __( 'Search Press News', 'your-plugin-textdomain' ),
       'parent_item_colon'  => __( 'Parent Press News Item:', 'your-plugin-textdomain' ),
       'not_found'          => __( 'No Press News found.', 'your-plugin-textdomain' ),
       'not_found_in_trash' => __( 'No Press News found in Trash.', 'your-plugin-textdomain' )
   );

   $args = array(
       'labels'             => $labels,
       'public'             => true,
       'publicly_queryable' => true,
       'show_ui'            => true,
       'show_in_menu'       => true,
       'query_var'          => true,
       'capability_type'    => 'post',
       'menu_icon' => 'dashicons-admin-comments',
       'map_meta_cap'       => true,
       'has_archive'        => false,
       'hierarchical'       => false,
       'menu_position'      => null,
       'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt')
   );

   register_post_type('press-news', $args);

   $labels = array(
       'name'               => _x( 'Press Downloads', 'post type general name', 'your-plugin-textdomain' ),
       'singular_name'      => _x( 'Press Download', 'post type singular name', 'your-plugin-textdomain' ),
       'menu_name'          => _x( 'Press Downloads', 'admin menu', 'your-plugin-textdomain' ),
       'name_admin_bar'     => _x( 'Press Download', 'add new on admin bar', 'your-plugin-textdomain' ),
       'add_new'            => _x( 'Add Press Download', 'Person', 'your-plugin-textdomain' ),
       'add_new_item'       => __( 'Add New Press Download', 'your-plugin-textdomain' ),
       'new_item'           => __( 'New Press Download', 'your-plugin-textdomain' ),
       'edit_item'          => __( 'Edit Press Download', 'your-plugin-textdomain' ),
       'view_item'          => __( 'View PressDownload', 'your-plugin-textdomain' ),
       'all_items'          => __( 'All Press Downloads', 'your-plugin-textdomain' ),
       'search_items'       => __( 'Search Downloads', 'your-plugin-textdomain' ),
       'parent_item_colon'  => __( 'Parent Press Download:', 'your-plugin-textdomain' ),
       'not_found'          => __( 'No Press Downloads found.', 'your-plugin-textdomain' ),
       'not_found_in_trash' => __( 'No Press Downloads found in Trash.', 'your-plugin-textdomain' )
   );

   $args = array(
       'labels'             => $labels,
       'public'             => false,
       'publicly_queryable' => true,
       'show_ui'            => true,
       'show_in_menu'       => true,
       'query_var'          => true,
       'capability_type'    => 'post',
       'menu_icon' => 'dashicons-format-gallery',
       'map_meta_cap'       => true,
       'has_archive'        => false,
       'hierarchical'       => true,
       'menu_position'      => null,
       'supports'           => array('title', 'editor', 'author', 'thumbnail', 'excerpt')
   );

   register_post_type('press-download', $args);

   $labels = array(
       'name'               => _x( 'Speakers', 'post type general name', 'your-plugin-textdomain' ),
       'singular_name'      => _x( 'Speaker', 'post type singular name', 'your-plugin-textdomain' ),
       'menu_name'          => _x( 'Speakers', 'admin menu', 'your-plugin-textdomain' ),
       'name_admin_bar'     => _x( 'Speaker', 'add new on admin bar', 'your-plugin-textdomain' ),
       'add_new'            => _x( 'Add Speaker', 'Person', 'your-plugin-textdomain' ),
       'add_new_item'       => __( 'Add New Speaker', 'your-plugin-textdomain' ),
       'new_item'           => __( 'New Speaker', 'your-plugin-textdomain' ),
       'edit_item'          => __( 'Edit Speaker', 'your-plugin-textdomain' ),
       'view_item'          => __( 'View Speaker', 'your-plugin-textdomain' ),
       'all_items'          => __( 'All Speakers', 'your-plugin-textdomain' ),
       'search_items'       => __( 'Search Speakers', 'your-plugin-textdomain' ),
       'parent_item_colon'  => __( 'Parent Speaker:', 'your-plugin-textdomain' ),
       'not_found'          => __( 'No Speakers found.', 'your-plugin-textdomain' ),
       'not_found_in_trash' => __( 'No Speakers found in Trash.', 'your-plugin-textdomain' )
   );

   $args = array(
       'labels'             => $labels,
       'public'             => false,
       'publicly_queryable' => true,
       'show_ui'            => true,
       'show_in_menu'       => true,
       'query_var'          => true,
       'capability_type'    => 'post',
       'menu_icon' => 'dashicons-universal-access-alt',
       'map_meta_cap'       => true,
       'has_archive'        => false,
       'hierarchical'       => true,
       'menu_position'      => null,
       'supports'           => array('title', 'editor', 'author', 'thumbnail', 'excerpt')
   );

   register_post_type('speaker', $args);

 }

 ?>
