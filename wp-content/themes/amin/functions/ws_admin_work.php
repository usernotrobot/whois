<?php
/**
 * Defines new work post type
 * Adds metabox and stylesheet to new work admin
 * Moves old 'work' CPT to a subcategory of new 'work'
 *
 * Another hackjob by Wordsmith
 */



function gb_register_metaboxes()
{
		add_meta_box( /* Additional 'Publish' button */
		'gb_img_metabox2',
		'Publish Button',
		'gb_img_display_metabox2',
		'Work',
		'normal',
		'high'
	);
	
	add_meta_box( /* Example layout */
		'gb_img_metabox',
		'Example Layout',
		'gb_img_display_metabox',
		'Work',
		'side',
		'high'
	);

}
add_action( 'admin_init', 'gb_register_metaboxes' );


function gb_img_display_metabox2()
{
	echo '
		<style type="text/css">
		#gb_img_metabox2 {
			background: transparent;
			border: 0;
			box-shadow: none;
		}

		#gb_img_metabox2 > .handlediv,
		#gb_img_metabox2 > .hndle {
			display: none;
		}
		</style>
		
		<!-- publish button for bottom of page -->
		<input name="save" type="submit" class="button button-primary button-large" id="publish" value="Publish" />	
		
		<!-- arrows --> <!-- removed because positioning is impossible with error messages
		<img src="/wp-content/uploads/workadmin/a1.png" id="arrow1" />
		<img src="/wp-content/uploads/workadmin/a2.png" id="arrow2" />-->'	
;
}

function gb_img_display_metabox()
{
	echo '
		<style type="text/css">
		#gb_img_metabox {
			background: transparent;
			border: 0;
			box-shadow: none;
		}

		#gb_img_metabox > .handlediv,
		#gb_img_metabox > .hndle {
			display: none;
		}
		</style>
		
		<h2>Example Layout</h2>
		<img src="/wp-content/uploads/workadmin/examplelayout_fairmont.jpg" id="example-image" />'
;
}




function gb_img_admin_enqueue_scripts( $hook )
{
	global $post;

	if( is_admin() && ( $hook == 'post.php' || $hook == 'post-new.php' ) )
	{
		if( 
            		( !empty( $post ) && $post->post_type == 'work' ) ||
            		( !empty( $_GET['post_type'] ) && $_GET['post_type'] == 'work' )
        	)
		{
			wp_enqueue_style( 'ws_workadmin', get_stylesheet_directory_uri() . '/assets/css/ws_workadmin.css', array(), '1.0' );
		}
	}
}
add_action( 'admin_enqueue_scripts', 'gb_img_admin_enqueue_scripts' );