<?php
/**
 * Setup all functions.
 * Uncomment the required functionality and see specific function file
 * for addition configuration.
 *
 * @since TJ Blank 1.0
 * @return void
 */

/**
 * SOIL UTILITIES
 * Utilities to tidy up wordpress front-end :)
 * SOURCE: https://github.com/roots/soil
 */
// Load jQuery from the Google CDN
add_theme_support('soil-jquery-cdn');
// Cleaner WordPress markup
add_theme_support('soil-clean-up');
// Cleaner walker for navigation menus
add_theme_support('soil-nav-walker');
// Root relative URLs
add_theme_support('soil-relative-urls');
// Google Analytics (more info)
// add_theme_support('soil-google-analytics', 'UA-XXXXX-Y');
// Move all JS to the footer
add_theme_support('soil-js-to-footer');
// Disable trackbacks
add_theme_support('soil-disable-trackbacks');
// Disable asset versioning
add_theme_support('soil-disable-asset-versioning');


//Add support for post thmbnaiils
add_theme_support('post-thumbnails');

//Register a Main Menu that can be edited through the wordpress admin
register_nav_menu('Main','The main navigation');

//Use this to add data that will be added and provided by all the context when calling Timber::get_context();
add_filter('timber_context','add_to_context');

function add_to_context($context) {
	$context['option'] = get_fields('options');
	$context['newsletter_form'] = '[gravityform id=1 title=false description=false ajax=true]';

	$context['members_form'] = '[gravityform id=3 ajax=true]';
	if(get_the_ID()) {
		$postID = get_the_ID();
	} else {
		$postID = get_queried_object_id();
	}

    $post = new TimberPost($postID);

	$post = Timber::get_post($postID);
	$context['postID'] = $postID;
        //This will grab the first menu generated, if you want a specific menu simply add the menu slug
        if (is_user_logged_in() && is_nav_menu( 'logged-in' )) {
            $context['menu'] = new TimberMenu('logged-in'); 
        }
        else {
            $context['menu'] = new TimberMenu('Main Nav'); 
        }
	$context['mobileMenu'] = new TimberMenu('Mobile Nav'); //For some reason 'mobile-menu' breaks shit?
	//Menu doesn't apply correct class on archive pages
	$context['uriSeg'] = $_SERVER['REQUEST_URI'];
	$context['show_network_dropdown'] = show_network_dropdown();
	$context['breadcrumbs'] = assembleBreadcrumbs();
	$context['is_admin'] = is_user_logged_in();
	$ip = $_SERVER['REMOTE_ADDR'];
	if($ip == '84.45.38.132' || '::1'){
		$context['iptrue'] = TRUE;
	}
	$context['agency_standards_title'] = get_field('section_title', 'option');
	$context['agency_standards_description'] = get_field('section_description', 'option');
	$context['agency_standards_pods'] = get_field('numbered_pods', 'option');

    if ($post->slug == 'emea' || $post->slug == 'apac' || $post->slug == 'americas' || $post->post_type == 'agency') {
        $context['is_network'] = true;
    }

	if(get_field('breadcrumb_tone')) {
		$context['breadcrumb_tone'] = get_field('breadcrumb_tone');
	}

	$context['cf_ip'] = ( isset($_SERVER["HTTP_CF_IPCOUNTRY"]) ? $_SERVER["HTTP_CF_IPCOUNTRY"] : '' );

	return $context;
}

function show_network_dropdown() {
	global $post;
	if($post) {
		if($post->ID == 208 || $post->ID == 246 || $post->ID == 249 || $post->ID == 252) { // These are the IDs for the network page and its network areas
			return true;
		} else {
			return false;
		}
	}
}

if(function_exists('acf_add_options_page')) {
	if (is_user_logged_in()) {
		$current_user = wp_get_current_user();
		if($current_user->roles[0] == 'administrator'){
			acf_add_options_page();
		}
	}
}

add_action('after_setup_theme', 'theme_setup');
function theme_setup() {

	// Setup Custom Post types.
	add_action('init', 'custom_post_type');

	// Setup Custom Taxonomies.
	add_action('init', 'custom_taxonomy');

	//Defines user roles
	add_action('init', 'custom_user_role');

	// Setup enqueue scripts
	add_action('wp_enqueue_scripts', 'add_scripts');
}

// Removal of comments
// Removes from admin menu
add_action( 'admin_menu', 'my_remove_admin_menus' );
function my_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}
// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
// Removes from admin bar
function mytheme_admin_bar_render() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );


//removing that stuff from the dashboard
function rwp_remove_dashboard_meta() {
	remove_meta_box('dashboard_activity', 'dashboard', 'normal');
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
	remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
	remove_meta_box('dashboard_primary', 'dashboard', 'normal');
	remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
	remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
	remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
}
add_action('admin_init', 'rwp_remove_dashboard_meta');

function my_login_logo() { ?>
    <style type="text/css">
        .login h1 a {
            background-image: url(/wp-content/themes/amin/assets/img/amin-logo.svg);
            width: 220px;
            background-size: 100%;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function admin_customise() {
	echo '<style type="text/css">#wpfooter{ display: none!important; }</style>';
}
add_action('admin_head', 'admin_customise');

function assembleBreadcrumbs() {
	global $post;

	$breadcrumbs = array();

	if($post) {
		if(is_single($post->ID)) {
			if(get_post_type() == 'work'){
				if(isset($_GET['ag'])){
					$breadcrumbs = array(Timber::get_post($_GET['ag']), Timber::get_post($post->ID));
				} else {
					$breadcrumbs = array(Timber::get_post(201), Timber::get_post($post->ID)); // ML: 201 is the Work page.
				}
			} else if (get_post_type() == 'agency') {
				$breadcrumbs = array(array(Timber::get_post(208), Timber::get_post(252), Timber::get_post(246), Timber::get_post(249)));
			} elseif(get_post_type() == 'person'){
				if(get_field('leadership_team', $post->ID)){
					$breadcrumbs = array(Timber::get_post(211), Timber::get_post($post->ID)); // ML: 211 is 'Our Story'
				} else {
					$breadcrumbs = array(Timber::get_post(211), Timber::get_post(1866), Timber::get_post($post->ID)); // ML: 211 is 'Our Story' and 1866 is 'Agency Principles'
				}
			} else if(get_post_type() == 'press-news'){
				$breadcrumbs = array(Timber::get_post(5393), Timber::get_post(5395), Timber::get_post($post->ID)); // TW: 5370 is news listing page. 5368 is 'Press Room'.
			} else if(get_post_type() == 'event'){
				$breadcrumbs = array(Timber::get_post(5967), Timber::get_post($post->ID)); // TW: 5967 is the Events page.
			} else {
				if(isset($_GET['ag'])){
					$breadcrumbs = array(Timber::get_post($_GET['ag']), Timber::get_post($post->ID)); // ML: The $_GET is the agency ID
				} else {
					$breadcrumbs = array(Timber::get_post(2454), Timber::get_post($post->ID)); // ML: 2454 is news listing page.
				}
			}
		} else if(show_network_dropdown()) {
			if($post->ID == 208) {
				//Network page
				$breadcrumbs = array(array(Timber::get_post(208), Timber::get_post(252), Timber::get_post(246), Timber::get_post(249)));
			} else {
				//Network sub page
				$breadcrumbs = array(Timber::get_post(208), array(Timber::get_post(252), Timber::get_post(246), Timber::get_post(249)));

				$sub_pages = $breadcrumbs[1];

				//Move current page to front of array
				foreach ($sub_pages as $k => $v) {
					if($post->ID == $v->ID) {
						$move = $sub_pages[$k];
						unset($sub_pages[$k]);
						array_unshift($sub_pages, $move);
					}
				}
				//For structure to work, we need to wrap breadcrumbs in another array
				$breadcrumbs = array($sub_pages);
				array_unshift($breadcrumbs, Timber::get_post(208));
			}
		} else if(get_post_ancestors($post->ID)) {
			//Regular page with ancestors
			if(count(get_post_ancestors($post->ID)) == 1) {
				$breadcrumbs = array(Timber::get_post(get_post_ancestors($post->ID)[0]));
			} else {
				$breadcrumbs = array(Timber::get_post(get_post_ancestors($post->ID)));
			}
			array_push($breadcrumbs, Timber::get_post($post->ID));
		} else if(is_post_type_archive('post')) {
			// $post_archive = new stdClass();
			// $post_archive->post_name = 'news';
			// $post_archive->post_title = 'News';

			$breadcrumbs = array(Timber::get_post(214));
		} else {
			//Regular page with no ancestors
			$breadcrumbs = array(Timber::get_post($post->ID));
		}

	    return $breadcrumbs;
	}
}

//Determines settings for query on news archive page
function news_query( $query ) {
	global $paged;
    //print_r($query);
	if ($query->is_post_type_archive('post') && !is_admin()) {
		$query->set('posts_per_page', 9);

        if (isset($_GET['cat']) && isset($_GET['regionSlug']) && isset($_GET['agencyID'])) {
            $authorID = $_GET['agencyID'];
            $catID = get_category_by_slug($_GET['cat'])->term_id;

            $query->set('cat', $catID);
            $query->set('author', $authorID);

            return $query;
        }

        if (isset($_GET['regionSlug']) && isset($_GET['agencyID'])) {
            $authorID = $_GET['agencyID'];

            $query->set('author', $authorID);

            return $query;
        }

		if(isset($_GET['cat'])) {
			$catID = get_category_by_slug($_GET['cat'])->term_id;
			// print_r($catID);
			if($catID) {
				$query->set('cat', $catID);
			}
            return $query;
		}
        if (isset($_GET['regionSlug'])) {
            //$regionID = get_category_by_slug($_GET['region'])->term_id;
            $regionID = get_term_by('slug',$_GET['regionSlug'], 'region')->term_id;
            //print_r($regionID);

            if ($regionID) {
                $taxQuery = array(
                    array(
                        'taxonomy' => 'region',
                        'field' => 'id',
                        'terms' => $regionID,
                        'operator' => 'IN'
                    )
                );
                $query->set('tax_query', $taxQuery);
                return $query;
            }
        }
	}
}
add_action( 'pre_get_posts', 'news_query' );

add_action('save_post', 'remove_agency_map_transient');
add_action('post_updated', 'remove_agency_map_transient');

//add custom image sizes
add_image_size('head_shot', 250, 270, true);
add_image_size('client_logo', 270, 150, false);
add_image_size('news_listing_image', 340, 220, array('center', 'center'));

//Attempt at unlimited height hack
add_image_size('news_image', 715, 9999, false);
add_image_size('isotope_listing_image', 9999, 450, false);

//Gforms spinner url
add_filter( 'gform_ajax_spinner_url', 'spinner_url', 10, 2 );
function spinner_url( $image_src, $form ) {
    return get_template_directory_uri() . "/assets/img/icons/spinner.svg";
}

// Horrible hack to remove event from the url for cpt event
// http://wordpress.stackexchange.com/questions/203951/remove-slug-from-custom-post-type-post-urls
function na_remove_slug( $post_link, $post, $leavename ) {

    if ( 'event' != $post->post_type || 'publish' != $post->post_status ) {
        return $post_link;
    }

    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );

    return $post_link;
}
add_filter( 'post_type_link', 'na_remove_slug', 10, 3 );

function na_parse_request( $query ) {

    if ( ! $query->is_main_query() || 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        return;
    }

    if ( ! empty( $query->query['name'] ) ) {
        $query->set( 'post_type', array( 'post', 'event', 'page' ) );
    }
}
add_action( 'pre_get_posts', 'na_parse_request' );

//save hook to delete transients whenever an agency, or person is updated.
function delete_network_search_transient($postID) {
    $post_type = get_post_type($postID);

    if ($post_type == 'person' || $post_type == 'agency') {
        $transient_key = '_who-is-who_agencies';
        delete_transient($transient_key);
    }
}

add_action('save_post', 'delete_network_search_transient');

function wpb_disable_feed() {
	wp_die( __('No feed available, please visit the <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}
add_action('do_feed', 'wpb_disable_feed', 1);
add_action('do_feed_rdf', 'wpb_disable_feed', 1);
add_action('do_feed_rss', 'wpb_disable_feed', 1);
add_action('do_feed_rss2', 'wpb_disable_feed', 1);
add_action('do_feed_atom', 'wpb_disable_feed', 1);
add_action('do_feed_rss2_comments', 'wpb_disable_feed', 1);
add_action('do_feed_atom_comments', 'wpb_disable_feed', 1);
add_action('after_theme_support', 'remove_feed');
function remove_feed() {
   remove_theme_support('automatic-feed-links');
}
remove_action('wp_head', 'feed_links_extra', 3 );
remove_action('wp_head', 'feed_links', 2 );

function canonicalHandler() {
	if(is_tax('region')) {
		return "https://www.aminworldwide.com/network/";
	} else if(is_archive('award') ||
			  is_archive('company') ||
			  is_archive('new-clients') ||
			  is_archive('other') ||
			  is_tax('position-group') ||
			  is_tax('work-type') ||
			  is_singular('country')) {
		return "https://www.aminworldwide.com/";
	} else if(is_singular('client')) {
		return "https://www.aminworldwide.com/news/";
	} else {
		//
	}
}
add_filter('wpseo_canonical', 'canonicalHandler');
