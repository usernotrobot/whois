<?php

function add_scripts() {
    //de-register jquery version and then add one from google cdn
    wp_deregister_script('jquery');
    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js', false, '2.2.0');
    wp_enqueue_script('jquery');
	//Load in a main js folder with no depedancies, no version and added in the footer
	wp_enqueue_script('vendor', get_stylesheet_directory_uri() . '/assets/js/vendor.min.js', array('jquery'), null, true);
	wp_enqueue_script('main', get_stylesheet_directory_uri() . '/assets/js/bundle.min.js', array('jquery', 'vendor'), null, true);

	if(strpos($_SERVER['HTTP_HOST'], '.local')) {
		//Regular css file for local w/sourcemaps
		wp_enqueue_style('style', get_stylesheet_directory_uri() . '/assets/css/style.css');
	} else {
		//Minified version, no sourcemaps for speed
		wp_enqueue_style('style', get_stylesheet_directory_uri() . '/assets/css/style.min.css');
	}
	
	wp_enqueue_style('ws_work', get_stylesheet_directory_uri() . '/assets/css/ws_work.css');
        
	wp_enqueue_style('oswald', 'https://fonts.googleapis.com/css?family=Oswald:300,400,700');
    //wp_enqueue_script('mapbox', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.14.1/mapbox-gl.js', array('main'), null, true);
    wp_enqueue_script('mapbox', 'https://api.mapbox.com/mapbox-gl-js/v0.32.1/mapbox-gl.js', array('main'), null, true);
    //Decided to add this in manually so it's all in 1 stylesheet only
    //wp_enqueue_style('mapboxcss', 'https://api.tiles.mapbox.com/mapbox-gl-js/v0.14.1/mapbox-gl.css');

    wp_enqueue_script('isotope', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.2.2/isotope.pkgd.min.js', array('jquery', 'main'), null, true);

    //This will grab all the agencies based on the region of the current page you're on and that is it network page
    localizeAgencies();
    localiseClients();
	localiseNews();
    localiseNetworkSearchData();

    gravity_form_enqueue_scripts(4, true);
    gravity_form_enqueue_scripts(2, true);

}

function custom_admin_js($hook) {
    if ('post.php' == $hook) {
        wp_enqueue_script('admin', get_template_directory_uri() . '/src/js/admin.js', array('jquery'), null, true);
    }
    
    wp_enqueue_style( 'ws_dmin', get_stylesheet_directory_uri() . '/assets/css/ws_admin.css', array(), '1.0' );
}
add_action('admin_enqueue_scripts', 'custom_admin_js');

add_filter('acf/prepare_field/name=regional_background_image', 'my_acf_prepare_field');

function my_acf_prepare_field($hook){
    $screen = get_current_screen();
    if ( $screen->parent_base == 'edit' && get_post_type() == "agency") {
        $hook['preview_size'] = 'medium';
    }
    return $hook;
}