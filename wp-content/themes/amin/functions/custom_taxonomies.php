<?php
/**
 * Register custom taxonomies
 *
 * @since TJ Blank 1.0
 * @return void
 */


function custom_taxonomy() {

	$labels = array(
		'name'              => _x( 'Region', 'taxonomy general name' ),
		'singular_name'     => _x( 'Region', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Region' ),
		'all_items'         => __( 'All Region' ),
		'parent_item'       => __( 'Parent Region' ),
		'parent_item_colon' => __( 'Parent Region:' ),
		'edit_item'         => __( 'Edit Region' ),
		'update_item'       => __( 'Update Region' ),
		'add_new_item'      => __( 'Add New Region' ),
		'new_item_name'     => __( 'New Region Name' ),
		'menu_name'         => __( 'Region' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'regions' ),
	);

	register_taxonomy('region', array('agency', 'page', 'post', 'event', 'client', 'person', 'press-news'), $args);

	$labels = array(
		'name'              => _x( 'Country', 'taxonomy general name' ),
		'singular_name'     => _x( 'Country', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Countries' ),
		'all_items'         => __( 'All Countries' ),
		'parent_item'       => __( 'Parent Country' ),
		'parent_item_colon' => __( 'Parent Country:' ),
		'edit_item'         => __( 'Edit Country' ),
		'update_item'       => __( 'Update Country' ),
		'add_new_item'      => __( 'Add New Country' ),
		'new_item_name'     => __( 'New Country Name' ),
		'menu_name'         => __( 'Country' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'country' ),
	);

	register_taxonomy('country', array('agency'), $args);

	$labels = array(
		'name'              => _x( 'Work Type', 'taxonomy general name' ),
		'singular_name'     => _x( 'Work Type', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Work Types' ),
		'all_items'         => __( 'All Work Types' ),
		'parent_item'       => __( 'Parent Work Type' ),
		'parent_item_colon' => __( 'Parent Work Type:' ),
		'edit_item'         => __( 'Edit Work Type' ),
		'update_item'       => __( 'Update Work Type' ),
		'add_new_item'      => __( 'Add New Work Type' ),
		'new_item_name'     => __( 'New Work Type' ),
		'menu_name'         => __( 'Work Type' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'work-type' ),
	);

	register_taxonomy('work-type', array('work'), $args);

	$labels = array(
		'name'              => _x( 'Press News Type', 'taxonomy general name' ),
		'singular_name'     => _x( 'Press News Type', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Press News Types' ),
		'all_items'         => __( 'All Press News Types' ),
		'parent_item'       => __( 'Parent Press News Type' ),
		'parent_item_colon' => __( 'Parent Press News Type:' ),
		'edit_item'         => __( 'Edit Press News Type' ),
		'update_item'       => __( 'Update Press News Type' ),
		'add_new_item'      => __( 'Add New Press News Type' ),
		'new_item_name'     => __( 'New Press News Type' ),
		'menu_name'         => __( 'Press News Type' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'press-news-type' ),
	);

	register_taxonomy('press-news-type', array('press-news'), $args);

	$labels = array(
		'name'              => _x( 'Press Download Type', 'taxonomy general name' ),
		'singular_name'     => _x( 'Press Download Type', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Press Download Types' ),
		'all_items'         => __( 'All Press Download Types' ),
		'parent_item'       => __( 'Parent Press Download Type' ),
		'parent_item_colon' => __( 'Parent Press Download Type:' ),
		'edit_item'         => __( 'Edit Press Download Type' ),
		'update_item'       => __( 'Update Press Download Type' ),
		'add_new_item'      => __( 'Add New Press Download Type' ),
		'new_item_name'     => __( 'New Press Download Type' ),
		'menu_name'         => __( 'Press Download Type' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'press-download-type' ),
	);

	register_taxonomy('press-download-type', array('press-download'), $args);

    $labels = array(
		'name'              => _x( 'Position Group', 'taxonomy general name' ),
		'singular_name'     => _x( 'Position Group', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Position Groups' ),
		'all_items'         => __( 'All Position Groups' ),
		'parent_item'       => __( 'Parent Position Group' ),
		'parent_item_colon' => __( 'Parent Position Group:' ),
		'edit_item'         => __( 'Edit Position Group' ),
		'update_item'       => __( 'Update Position Group' ),
		'add_new_item'      => __( 'Add New Position Group' ),
		'new_item_name'     => __( 'New Position Group Name' ),
		'menu_name'         => __( 'Position Group' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'position-group' ),
	);

	register_taxonomy('position-group', array('person'), $args);

};
