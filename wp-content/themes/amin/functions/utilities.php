<?php
/*
* This will extract the data wanted from a TimberTerm
* To be used with an array map
*/
function extractTerm($term) {
    return array(
        'name' => $term->name,
        'val' => $term->slug
    );
}

/*
* Order an array of objects by the object parameter, in this case 'post_title'. Use in conjuction with usort.
*/
function sortOnPostTitle($a, $b) {
    return strcmp($a->post_title, $b->post_title);
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  $mimes['eps'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// show admin bar only for admins
// if (!current_user_can('manage_options')) {
//     add_filter('show_admin_bar', '__return_false');
// }
// show admin bar only for admins and editors
// if (!current_user_can('edit_posts')) {
//     add_filter('show_admin_bar', '__return_false');
// }

function extractSVG($file) {
    if ($file) {
        if (strpos($file, 'svg') !== false) {
			$file = str_replace('http://', 'http://' . DB_USER . ':' . DB_PASSWORD . '@', $file);
            return file_get_contents($file);
        } else {
            return '<img src="' . $file . '" class="image-fallback">';
        }
    } else {
        return;
    }
}

/*
* Returns the value of the region taxonomy based on the post id passed.
*/
function getRegion($id) {
    $region = wp_get_post_terms($id, 'region');
    if(isset($region[0])) {
        return $region[0];
    }
}
/*
* Returns list of clients.
*/
function localiseClients() {
    $post = Timber::get_post();
	if($post) {
	    if ($post->ID == 205) {
	        $clients = Timber::get_posts(array(
	            'post_type' => 'client',
	            'posts_per_page' => -1,
	            'post_status' => 'publish'
	        ));

	        $active_filters = array_map(function($client) {
	            return $client->post_title[0];
	        }, $clients);

	        $client_filters = array_map(function($letter) use ($active_filters) {
	            return [
	                'name' => $letter,
	                'isActive' => in_array(strtoupper($letter), $active_filters)
	            ];
	        }, range('a', 'z'));

	        wp_localize_script('main', 'WP', array(
	            'client_filters' => $client_filters,
	            'clients' => $clients
	        ));
	    }
	}
}
/*
* This will get the agencies and then localize that data making it available
* to an scripts we want it to be available for.
*/
function localizeAgencies() {
    $post = Timber::get_post();
    //these are the network pages
	if($post) {
	    if ($post->ID == 246 ||  $post->ID == 249 || $post->ID == 252) {
	        $region = getRegion($post->ID);
	        $timberRegion = new TimberTerm($region->term_id);

	        $transientKey = 'amin_agenciesGeoJson_' . $region->slug;

	        $agencyTransient = get_transient($transientKey);

	        if (empty($agencyTransient)) {
				$agencies = $timberRegion->get_posts(array(
					'posts_per_page' => -1,
					'post_type' => 'agency',
					'post_status' => 'publish'
				));
	            $agenciesGeoJson = agenciesToGeojson($agencies);
	            set_transient($transientKey, $agenciesGeoJson, 24 * HOUR_IN_SECONDS);
	        } else {
	            $agenciesGeoJson = $agencyTransient;
	        }

	        wp_localize_script('main', 'WP', array(
	            'region' => $region->slug,
	            'agencies' => $agenciesGeoJson,
	            'recruitingCountries' => countriesToGeojson(getRecruitingCountries($region))
	        ));
	    } else if ($post->ID == 407) { // this is the member page

	        $transient = get_transient('amin_member_mapdata');

	        if (empty($transient)) {
	            $americas = get_term(3, 'region');
	            $emea = get_term(4, 'region');
	            $apac = get_term(2, 'region');

	            $recruitingCountries = array();
	            $americasArray = getRecruitingCountries($americas);
	            $emeaArray = getRecruitingCountries($emea);
	            $apacArray = getRecruitingCountries($apac);

	            $recruitingCountries = array_merge($americasArray, $emeaArray, $apacArray);

	            wp_localize_script('main', 'WP', array(
	                'memberRecruitingCountries' => countriesToGeojson($recruitingCountries)
	            ));

	            set_transient('amin_member_mapdata', countriesToGeojson($recruitingCountries), 24 * HOUR_IN_SECONDS);
	        } else {
	            wp_localize_script('main', 'WP', array(
	                'memberRecruitingCountries' => $transient
	            ));
	        }
	    }
	}
}

function localizeAgenciesByWholeWorld() {
    $posts = Timber::get_posts( array(246, 249, 252) );
    
    $wholeWorld = array();
           
    foreach ($posts as $post) {
        $region = getRegion($post->ID);
	$timberRegion = new TimberTerm($region->term_id);

	$transientKey = 'amin_agenciesGeoJson_' . $region->slug;

        $agencyTransient = get_transient($transientKey);

        if (empty($agencyTransient)) {
                        $agencies = $timberRegion->get_posts(array(
                                'posts_per_page' => -1,
                                'post_type' => 'agency',
                                'post_status' => 'publish'
                        ));
            $agenciesGeoJson = agenciesToGeojson($agencies);
            set_transient($transientKey, $agenciesGeoJson, 24 * HOUR_IN_SECONDS);
        } else {
            $agenciesGeoJson = $agencyTransient;
        }
        
        $wholeWorld[] = array(
            'region' => $region->slug,
            'agencies' => $agenciesGeoJson,
            'recruitingCountries' => countriesToGeojson(getRecruitingCountries($region))
        );
    }
    
    return $wholeWorld;
}
/*
* Gets all the news posts. This could be refactored.
*/
function localiseNews() {
    if (is_page('news') || is_page('press-releases') || is_page('awards')) {
		if(is_page('news')) {
			$news = Timber::get_posts(array(
	    		'post_type' => 'post',
	    		'posts_per_page' => -1,
	    		'post_status' => 'publish'
	    	));
		} else if(is_page('press-releases')) {
			$news = Timber::get_posts(array(
	    		'post_type' => 'press-news',
	    		'posts_per_page' => -1,
	    		'post_status' => 'publish',
                'tax_query' => array(
                    array(
                        'taxonomy'  => 'press-news-type',
                        'field'     => 'slug',
                        'terms'     => 'press-release'
                    )
                )
	    	));
		} else if(is_page('awards')) {
            $news = Timber::get_posts(array(
	    		'post_type' => 'press-news',
	    		'posts_per_page' => -1,
	    		'post_status' => 'publish',
                'tax_query' => array(
                    array(
                        'taxonomy'  => 'press-news-type',
                        'field'     => 'slug',
                        'terms'     => 'awards'
                    )
                )
	    	));
        };
        $newsPosts = array();
        foreach ($news as $key => $post) {
            $returnedPost = array();
    		$returnedPost['title'] = $post->post_title;
    		$returnedPost['intro'] = $post->sub_title;
    		$returnedPost['author'] = ( $post->author_override ? $post->author_override : $post->author->name );
    		$returnedPost['image'] = ( $post->get_field('hero_image')['sizes']['news_listing_image'] ? $post->get_field('hero_image')['sizes']['news_listing_image'] : get_template_directory_uri() . '/assets/img/backgrounds/placeholder-news-listing.jpg' );
    		$returnedPost['permalink'] = $post->path();
			$agency = get_posts(array('author' => $post->author->ID, 'post_type' => 'agency', 'post_status' => 'publish', 'posts_per_page' => 1));
			$returnedPost['agency'] = ( isset($agency[0]->post_title) ? $agency[0]->post_title : get_userdata($post->post_author)->data->display_name );
			$returnedPost['agency_permalink'] = ( isset($agency[0]) ? get_permalink($agency[0]->ID) : $post->path() );
            $returnedPost['region'] = ( $post->get_terms('region') ? $post->get_terms('region')[0]->slug : '' );
			//Originally used array_maps. Now using foreach for speed.
			$cats = array();
			foreach ($post->terms as $key => $cat) {
				array_push($cats, $cat->slug);
			}
			$returnedPost['categories'] = implode(" ", $cats);
			$tags = array();
			foreach ($post->tags as $key => $tag) {
				array_push($tags, $tag->slug);
			}
			$returnedPost['tags'] = implode(" ", $tags);
    	    array_push($newsPosts, $returnedPost);
        }

		if(is_page('news')) {
			if(get_transient('news_categories')) {
				$categories = get_transient('news_categories');
			} else {
				$categories = Timber::get_terms('category');
				set_transient('news_categories', $categories, 24 * HOUR_IN_SECONDS);
			}
		} else {
            $categories = '';
        }

		if(get_transient('regions')) {
			$regions = get_transient('regions');
		} else {
			$regions = Timber::get_terms('region', array('orderby' => 'count', 'order' => 'DESC'));
			$regions = array_map(function($region) {
				$region->name = html_entity_decode($region->name);
				return $region;
			}, $regions);
			set_transient('regions', $regions, 24 * HOUR_IN_SECONDS);
		}

        function if_agency_slug_in_posts($posts, $slug) {
            foreach ($posts as $key => $post) {
                if($post['agency'] == $slug) {
                    return true;
                }
            }
        }

		$agencies = Timber::get_posts(array(
			'post_type' => 'agency',
    		'posts_per_page' => -1,
    		'post_status' => 'publish'
        ));
        $agencyFilters = array();
        foreach ($agencies as $key => $agency) {
            $returnedPost = array();
            $title = html_entity_decode($agency->get_title());
            // $slug = strtolower(implode("", explode(" ", $agency->get_title())));
            $slug = str_replace('-', '', $agency->post_name);

            if(if_agency_slug_in_posts($newsPosts, $slug)) {
    			$returnedPost['name'] = $title;
    			$returnedPost['slug'] = $slug;
    			$returnedPost['region'] = $agency->get_terms('region')[0]->slug;
            } else {
				continue;
			}
			array_push($agencyFilters, $returnedPost);
        }

		// delete_transient('amin_news');
		// $transientKey = 'amin_news';
		//
		// $newsTransient = get_transient($transientKey);

		// if (empty($newsTransient)) {
		// 	set_transient($transientKey, $newsPosts, 24 * HOUR_IN_SECONDS);
		// } else {
		// 	$newsPosts = $newsTransient;
		// }

    	wp_localize_script('main', 'WP', array(
    		'news' => $newsPosts,
			'categories' => $categories,
			'regions' => $regions,
			'agencyFilters' => $agencyFilters
    	));
    }
}

/**
 * Assembles the person data used by localisePeople.
 * @param	Integer	$id
 * @return 	Array $data
 */
function getPersonData($person, $get_related = false) {
	$agency = Timber::get_post($person->agency);
	$position_group = ($person->get_terms('position-group') ? wp_list_pluck($person->get_terms('position-group'), 'name') : '');

	//prevents infinite loop
	if($get_related) {
		$related = array();
		$related_people = Timber::get_posts(array(
			'post_type' => 'person',
			'post__not_in' => array($person->ID),
			'meta_query' => array(
				array(
					'key' => 'agency',
					'value' => $agency->ID,
					'compare' => '='
				)
			)
		));
		foreach ($related_people as $key => $r) {
			$related[] = getPersonData($r);
		}
	} else {
		$related = '';
	}

	$data =  array(
		'ID' => $person->ID,
		'name' => $person->post_title,
		'position' => $person->position,
		'position_group' => $position_group,
		'description' => $person->description,
		'link' => $person->link(),
		'hero_image' => $person->get_field('hero_image'),
		'person_image' => $person->get_field('image'),
		'agency' => $agency->post_title,
		'agency_logo' => $agency->get_field('logo'),
		'phone' => $person->telephone,
		'email' => $person->email,
		'related' => $related
	);
	return $data;
}

/**
 * We want to be able to search for memebers / people so lets find and collect all of them and then localise them into a script on the page
 * @return Array    $data
 */
function localisePeople() {
    $data = array();
    //arguments for the wp query
    $args = array(
        'post_type' => 'person',
        'post_status' => 'publish',
        'posts_per_page' => -1
    );
    $people = Timber::get_posts($args);

    //loop through each one and extract / fetch extra data we need / want
    foreach ($people as $person) {
        array_push($data, getPersonData($person, true));
    }

    return $data;
}

/**
 * Works similar to how localizeAgencies works but that depends on the region they belong too
 * I just want all of them.
 * @return Array $data
 */
function getAgencies() {
    $data = array(
        'agencies' => array(),
        'countries' => array()
    );

    //arguments for the wp query
    $args = array(
        'post_type' => 'agency',
        'post_status' => 'publish',
        'posts_per_page' => -1
    );
    $agencies = Timber::get_posts($args);

    foreach ($agencies as $agency) {
		if(isset($agency->get_terms('country')[0])) {
			$country = $agency->get_terms('country')[0];

			array_push($data['agencies'], array(
	            'ID' => $agency->ID,
	            'name' => $agency->post_title,
	            'link' => $agency->link(),
	            'country' => (isset($agency->get_terms('country')[0]) ? $agency->get_terms('country')[0] : '' )
	        ));

			//we'll make a list of all the countries here, that way we get all the data and associated data in one lookup
	        array_push($data['countries'], array(
	            'ID' => $country->ID,
	            'name' => $country->name,
	            'agencyLink' => $agency->link(),
	            'slug' => $country->slug
	        ));
		}
    }

    return $data;
}

/**
 * Calls on the other data collection techniques and then localises all that data
 * @return null
 */
function localiseNetworkSearchData() {
    if (is_page('who-is-who')) {
        $wholeWorld =  array('wholeWorld' => localizeAgenciesByWholeWorld());

        //lets transient this data, because it should change too often.
        $transient_key = 'network_search';
        $transient = get_transient($transient_key);

        if (empty($transient)) {
            $data = array(
                'people' => localisePeople(),
                'agenciesData' => getAgencies(),
                'wholeWorld' => $wholeWorld,
            );

            set_transient($transient_key, $data, HOUR_IN_SECONDS);
            wp_localize_script('main', 'WP', $data);
        } else {
            $data = array_merge($transient, $wholeWorld);
            wp_localize_script('main', 'WP', $data);
        }
    }
}

/**
 * Gets a user's location from the Cloudflare IP header.
 * If a user's country is recruiting, they'll get the network page message.
 * @param $post_title
 * @return Boolean | String
 */
function myCountryIsRecruiting($post_title) {
	$mycountry_is_recruiting = false;
	// depends on cloudflare
	if(isset($_SERVER['HTTP_CF_IPCOUNTRY'])) {
		$countrycode = $_SERVER['HTTP_CF_IPCOUNTRY'];
		$countries = array( 'AF' => 'Afghanistan', 'AX' => 'Aland Islands', 'AL' => 'Albania', 'DZ' => 'Algeria', 'AS' => 'American Samoa', 'AD' => 'Andorra', 'AO' => 'Angola', 'AI' => 'Anguilla', 'AQ' => 'Antarctica', 'AG' => 'Antigua And Barbuda', 'AR' => 'Argentina', 'AM' => 'Armenia', 'AW' => 'Aruba', 'AU' => 'Australia', 'AT' => 'Austria', 'AZ' => 'Azerbaijan', 'BS' => 'Bahamas', 'BH' => 'Bahrain', 'BD' => 'Bangladesh', 'BB' => 'Barbados', 'BY' => 'Belarus', 'BE' => 'Belgium', 'BZ' => 'Belize', 'BJ' => 'Benin', 'BM' => 'Bermuda', 'BT' => 'Bhutan', 'BO' => 'Bolivia', 'BA' => 'Bosnia And Herzegovina', 'BW' => 'Botswana', 'BV' => 'Bouvet Island', 'BR' => 'Brazil', 'IO' => 'British Indian Ocean Territory', 'BN' => 'Brunei Darussalam', 'BG' => 'Bulgaria', 'BF' => 'Burkina Faso', 'BI' => 'Burundi', 'KH' => 'Cambodia', 'CM' => 'Cameroon', 'CA' => 'Canada', 'CV' => 'Cape Verde', 'KY' => 'Cayman Islands', 'CF' => 'Central African Republic', 'TD' => 'Chad', 'CL' => 'Chile', 'CN' => 'China', 'CX' => 'Christmas Island', 'CC' => 'Cocos (Keeling) Islands', 'CO' => 'Colombia', 'KM' => 'Comoros', 'CG' => 'Congo', 'CD' => 'Congo, Democratic Republic', 'CK' => 'Cook Islands', 'CR' => 'Costa Rica', 'CI' => 'Cote D\'Ivoire', 'HR' => 'Croatia', 'CU' => 'Cuba', 'CY' => 'Cyprus', 'CZ' => 'Czech Republic', 'DK' => 'Denmark', 'DJ' => 'Djibouti', 'DM' => 'Dominica', 'DO' => 'Dominican Republic', 'EC' => 'Ecuador', 'EG' => 'Egypt', 'SV' => 'El Salvador', 'GQ' => 'Equatorial Guinea', 'ER' => 'Eritrea', 'EE' => 'Estonia', 'ET' => 'Ethiopia', 'FK' => 'Falkland Islands (Malvinas)', 'FO' => 'Faroe Islands', 'FJ' => 'Fiji', 'FI' => 'Finland', 'FR' => 'France', 'GF' => 'French Guiana', 'PF' => 'French Polynesia', 'TF' => 'French Southern Territories', 'GA' => 'Gabon', 'GM' => 'Gambia', 'GE' => 'Georgia', 'DE' => 'Germany', 'GH' => 'Ghana', 'GI' => 'Gibraltar', 'GR' => 'Greece', 'GL' => 'Greenland', 'GD' => 'Grenada', 'GP' => 'Guadeloupe', 'GU' => 'Guam', 'GT' => 'Guatemala', 'GG' => 'Guernsey', 'GN' => 'Guinea', 'GW' => 'Guinea-Bissau', 'GY' => 'Guyana', 'HT' => 'Haiti', 'HM' => 'Heard Island & Mcdonald Islands', 'VA' => 'Holy See (Vatican City State)', 'HN' => 'Honduras', 'HK' => 'Hong Kong', 'HU' => 'Hungary', 'IS' => 'Iceland', 'IN' => 'India', 'ID' => 'Indonesia', 'IR' => 'Iran, Islamic Republic Of', 'IQ' => 'Iraq', 'IE' => 'Ireland', 'IM' => 'Isle Of Man', 'IL' => 'Israel', 'IT' => 'Italy', 'JM' => 'Jamaica', 'JP' => 'Japan', 'JE' => 'Jersey', 'JO' => 'Jordan', 'KZ' => 'Kazakhstan', 'KE' => 'Kenya', 'KI' => 'Kiribati', 'KR' => 'Korea', 'KW' => 'Kuwait', 'KG' => 'Kyrgyzstan', 'LA' => 'Lao People\'s Democratic Republic', 'LV' => 'Latvia', 'LB' => 'Lebanon', 'LS' => 'Lesotho', 'LR' => 'Liberia', 'LY' => 'Libyan Arab Jamahiriya', 'LI' => 'Liechtenstein', 'LT' => 'Lithuania', 'LU' => 'Luxembourg', 'MO' => 'Macao', 'MK' => 'Macedonia', 'MG' => 'Madagascar', 'MW' => 'Malawi', 'MY' => 'Malaysia', 'MV' => 'Maldives', 'ML' => 'Mali', 'MT' => 'Malta', 'MH' => 'Marshall Islands', 'MQ' => 'Martinique', 'MR' => 'Mauritania', 'MU' => 'Mauritius', 'YT' => 'Mayotte', 'MX' => 'Mexico', 'FM' => 'Micronesia, Federated States Of', 'MD' => 'Moldova', 'MC' => 'Monaco', 'MN' => 'Mongolia', 'ME' => 'Montenegro', 'MS' => 'Montserrat', 'MA' => 'Morocco', 'MZ' => 'Mozambique', 'MM' => 'Myanmar', 'NA' => 'Namibia', 'NR' => 'Nauru', 'NP' => 'Nepal', 'NL' => 'Netherlands', 'AN' => 'Netherlands Antilles', 'NC' => 'New Caledonia', 'NZ' => 'New Zealand', 'NI' => 'Nicaragua', 'NE' => 'Niger', 'NG' => 'Nigeria', 'NU' => 'Niue', 'NF' => 'Norfolk Island', 'MP' => 'Northern Mariana Islands', 'NO' => 'Norway', 'OM' => 'Oman', 'PK' => 'Pakistan', 'PW' => 'Palau', 'PS' => 'Palestinian Territory, Occupied', 'PA' => 'Panama', 'PG' => 'Papua New Guinea', 'PY' => 'Paraguay', 'PE' => 'Peru', 'PH' => 'Philippines', 'PN' => 'Pitcairn', 'PL' => 'Poland', 'PT' => 'Portugal', 'PR' => 'Puerto Rico', 'QA' => 'Qatar', 'RE' => 'Reunion', 'RO' => 'Romania', 'RU' => 'Russian Federation', 'RW' => 'Rwanda', 'BL' => 'Saint Barthelemy', 'SH' => 'Saint Helena', 'KN' => 'Saint Kitts And Nevis', 'LC' => 'Saint Lucia', 'MF' => 'Saint Martin', 'PM' => 'Saint Pierre And Miquelon', 'VC' => 'Saint Vincent And Grenadines', 'WS' => 'Samoa', 'SM' => 'San Marino', 'ST' => 'Sao Tome And Principe', 'SA' => 'Saudi Arabia', 'SN' => 'Senegal', 'RS' => 'Serbia', 'SC' => 'Seychelles', 'SL' => 'Sierra Leone', 'SG' => 'Singapore', 'SK' => 'Slovakia', 'SI' => 'Slovenia', 'SB' => 'Solomon Islands', 'SO' => 'Somalia', 'ZA' => 'South Africa', 'GS' => 'South Georgia And Sandwich Isl.', 'ES' => 'Spain', 'LK' => 'Sri Lanka', 'SD' => 'Sudan', 'SR' => 'Suriname', 'SJ' => 'Svalbard And Jan Mayen', 'SZ' => 'Swaziland', 'SE' => 'Sweden', 'CH' => 'Switzerland', 'SY' => 'Syrian Arab Republic', 'TW' => 'Taiwan', 'TJ' => 'Tajikistan', 'TZ' => 'Tanzania', 'TH' => 'Thailand', 'TL' => 'Timor-Leste', 'TG' => 'Togo', 'TK' => 'Tokelau', 'TO' => 'Tonga', 'TT' => 'Trinidad And Tobago', 'TN' => 'Tunisia', 'TR' => 'Turkey', 'TM' => 'Turkmenistan', 'TC' => 'Turks And Caicos Islands', 'TV' => 'Tuvalu', 'UG' => 'Uganda', 'UA' => 'Ukraine', 'AE' => 'United Arab Emirates', 'GB' => 'United Kingdom', 'US' => 'United States', 'UM' => 'United States Outlying Islands', 'UY' => 'Uruguay', 'UZ' => 'Uzbekistan', 'VU' => 'Vanuatu', 'VE' => 'Venezuela', 'VN' => 'Viet Nam', 'VG' => 'Virgin Islands, British', 'VI' => 'Virgin Islands, U.S.', 'WF' => 'Wallis And Futuna', 'EH' => 'Western Sahara', 'YE' => 'Yemen', 'ZM' => 'Zambia', 'ZW' => 'Zimbabwe');
		$mycountry = $countries[$countrycode];

		$recruiting_countries_option = array_map(function($country) {
			return $country->name;
		}, get_field('recruiting_countries', 'option'));
		
		// We search the options array to see if our country is there. This only works if the big list above is the same as what we have in the taxonomy country list.
		if (in_array($mycountry, $recruiting_countries_option)) {
			$mycountry_is_recruiting = $mycountry;
		} else {
			$mycountry_is_recruiting = FALSE;
		}

		// $country_term = Timber::get_terms(array(
		// 	'name' => $mycountry,
		// 	'taxonomy' => 'country'
		// ))[0];
		// $country_region = get_term($country_term->region);

		// if(in_array($mycountry, $recruiting_countries_option)) {
		// 	if(strtolower($country_region->name) == strtolower($post_title)) {
		// 		$mycountry_is_recruiting = $mycountry;
		// 	}
		// }
	}
	return $mycountry_is_recruiting;
}

/*
* Gets all the countries and then returns them in a new array only if
* they are recruting and within the region of the one passed.
* This means that you will only get countries countries recruting within that region only.
*/
function getRecruitingCountries($region) {
        $transientKey = 'amin_recruitingAgencies_' . $region->slug;
        $transient = get_transient($transientKey);

        if (empty($transient)) {
            $recruitingCountries = array();
            //by using Timber::get_terms it will also get all ACF data for us too. making it super easy to compare
            $countries = Timber::get_terms('country');

            foreach($countries as $country) {
                if (isset($country->recruiting)) {
                    if ($country->region == $region->term_id && $country->recruiting == true) {
                        $country->regionName = $region->slug;
                        array_push($recruitingCountries, $country);
                    }
                }
            }
            set_transient($transientKey, $recruitingCountries, 24 * HOUR_IN_SECONDS);
            return $recruitingCountries;
        } else {
            return $transient;
        }
}

//agencies to geojson V2, now include addtional office locations
function agenciesToGeojson($agencies) {
    $locations = array();

    foreach($agencies as $agency) {
        //check if they have a primary location
        if (isset($agency->location['lng']) && isset($agency->location['lat'])) {
            $primary = array(
                'type' => 'Feature',
                'properties' => array(
                    // 'logo' => extractSVG($agency->get_field('logo')['url']),
                    'title' => $agency->post_title,
                    'description' => $agency->get_field('map_snippet'),
                    'link' => $agency->path,
                    'markerType' => 'agency',
                    'marker-size' => 'medium',
                    'marker-color' => '#f38e4e',
                    'marker-symbol' => 'marker'
                ),
                'geometry' => array(
                    'type' => 'Point',
                    'coordinates' => [$agency->location['lng'], $agency->location['lat']]
                )
            );
            //they pass, add it to the array
            array_push($locations, $primary);

            //now lets check if they have additional office locations
            $additional = $agency->get_field('additional_offices');
            if (!empty($additional)) {
                //loop through each one and then create a new point for it on the map and push it.
                foreach($additional as $office) {
                    if (isset($office['location']['lng']) && isset($office['location']['lat'])) {
                        $secondary = array(
                            'type' => 'Feature',
                            'properties' => array(
                                'title' => $agency->post_title,
                                'description' => $agency->get_field('map_snippet'),
                                'link' => $agency->path,
                                'markerType' => 'agency',
                                'marker-size' => 'medium',
                                'marker-color' => '#f38e4e',
                                'marker-symbol' => 'marker'
                            ),
                            'geometry' => array(
                                'type' => 'Point',
                                'coordinates' => [$office['location']['lng'], $office['location']['lat']]
                            )
                        );

                        array_push($locations, $secondary);
                    }
                }
            }
        }
    }

    //use array filter because it will return all values that are not null or false.
    return array(
        'type' => 'FeatureCollection',
        'features' => array_values(array_filter($locations))
    );
}

function countriesToGeojson($countries) {
    $features = array_map(function($country){
        if (isset($country->location['lng']) && isset($country->location['lat'])) {
            return array(
                'type' => 'Feature',
                'properties' => array(
                    'title' => $country->name,
                    'region' => $country->regionName,
                    'markerType' => 'recruiting',
                    'marker-size' => 'medium',
                    'marker-color' => '#f38e4e',
                    'marker-symbol' => 'marker'
                ),
                'geometry' => array(
                    'type' => 'Point',
                    'coordinates' => [$country->location['lng'], $country->location['lat']]
                )
            );
        } else {
            return;
        }
    }, $countries);
    return array(
        'type' => 'FeatureCollection',
        'features' => array_values(array_filter($features))
    );
}
/*
* Will sort a list of agencies based of the country term.
* Accepts a timber post and expects to have a country property
*/
function sortByCountry($a, $b) {
    return strcmp($a->country, $b->country);
}

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function custom_dashboard_widgets() {
    wp_add_dashboard_widget(
        'edit_agency_widget',         // Widget slug.
        'Edit Agency Profile',         // Title.
        'edit_agency_widget_function' // Display function.
    );
}

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function edit_agency_widget_function() {
    // Display whatever it is you want to show.
    echo "<style>#edit_agency_widget .handlediv, .ui-sortable-handle{display:none;}</style>";
    echo "<a href='/wp/wp-admin/edit.php?post_type=agency'>Edit your Agency profile</a><br><br>";
    echo "<a href='/wp/wp-admin/profile.php'>Edit your AMIN.com login</a>";
}

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function add_staff_form() {
    wp_add_dashboard_widget(
        'add_staff_widget',         // Widget slug.
        'Add staff member',         // Title.
        'add_staff_widget_function' // Display function.
    );
}

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function add_staff_widget_function() {
    $form = '[gravityform id=5 title=false description=false ajax=true]';
    echo "<style>#add_staff_widget .handlediv, .ui-sortable-handle{display:none;}#gform_5 #field_5_2 .ginput_container span{float:left;width:100%;}#gform_5 #field_5_2 .ginput_container span label{display:none;}#field_5_4,#field_5_5{display:none!important;}#input_5_3_container span label{display:none!important;}#wpseo-dashboard-overview{display:none!important;}</style>";
    echo "<strong>Add a staff member</strong>";
    echo do_shortcode($form);
}

function work_checker() {
    // here we check if the author has created a work post. If not, we remove the list in the CMS and replace with a message.
    if (isset($_GET['post_type']) && $_GET['post_type'] == 'work') {
        $current_user = wp_get_current_user();
        if($current_user->roles[0] == 'agency'){
        	global $wpdb;
        	$work = $wpdb->get_var("SELECT ID FROM wp_posts WHERE post_author = $current_user->ID AND post_type = 'work' LIMIT 1");
            if(!$work){
                echo "<style>.wp-list-table.posts,.tablenav.bottom,.tablenav.top,#screen-options-link-wrap{display:none!important;}</style>";
                echo "<div style='float: left;position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);'>You haven't added any work assets yet.</div>";
            }
        }
    }
}
add_action('admin_enqueue_scripts', 'work_checker');

function people_checker() {
    // here we check if the author has created a person post. If not, we remove the list in the CMS and replace with a message.
    if (isset($_GET['post_type']) && $_GET['post_type'] == 'person') {
        $current_user = wp_get_current_user();
        if($current_user->roles[0] == 'agency'){
        	global $wpdb;
        	$people = $wpdb->get_var("SELECT ID FROM wp_posts WHERE post_author = $current_user->ID AND post_type = 'person' LIMIT 1");
            if(!$people){
                echo "<style>.wp-list-table.posts,.tablenav.bottom,.tablenav.top,#screen-options-link-wrap{display:none!important;}</style>";
                echo "<div style='float: left;position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);'>You haven't added any people assets yet.</div>";
            }
        }
    }
}
add_action('admin_enqueue_scripts', 'people_checker');

function client_checker() {
    // here we check if the author has created a client post. If not, we remove the list in the CMS and replace with a message.
    if (isset($_GET['post_type']) && $_GET['post_type'] == 'client') {
        $current_user = wp_get_current_user();
        if($current_user->roles[0] == 'agency'){
        	global $wpdb;
        	$client = $wpdb->get_var("SELECT ID FROM wp_posts WHERE post_author = $current_user->ID AND post_type = 'client' LIMIT 1");
            if(!$client){
                echo "<style>.wp-list-table.posts,.tablenav.bottom,.tablenav.top,#screen-options-link-wrap{display:none!important;}</style>";
                echo "<div style='float: left;position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);'>You haven't added any client assets yet.</div>";
            }
        }
    }
}
add_action('admin_enqueue_scripts', 'people_checker');

function remove_agency_map_transient() {
	if(isset($_POST['post_ID'])) {
		$ID = $_POST['post_ID'];
	    $type = get_post_type($ID);
        $region = getRegion($ID);
		if ($type === 'agency') {
			delete_transient('amin_agenciesGeoJson_' . $region->slug);
		}
	}
}

//redirect user to 'Who is Who' after login
function custom_login_redirect( $redirect_to, $request, $user ) {
	$post = get_page_by_path('who-is-who');
	$url = (isset($post->ID) ? get_permalink($post->ID):false);
	if($url) {
		return $url;
	} else {
		return $redirect_to;
	}
}
add_filter( 'login_redirect', 'custom_login_redirect', 10, 3 );
