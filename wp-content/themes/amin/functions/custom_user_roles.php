<?php
/**
 * Define custom post types
 *
 * @since TJ Blank 1.0
 * @return void
 */

function custom_user_role() {
    $result = add_role(
	    'agency',
	    __( 'Agency' )
	);
	 $result = add_role(
	    'staff',
	    __( 'Staff' )
	);
    $result = add_role(
	    'pr-editor',
	    __( 'PR Editor' ),
		array(
			'delete_others_pages' => true,
			'delete_others_posts' => true,
			'delete_pages' => true,
			'delete_posts' => true,
			'delete_private_pages' => true,
			'delete_private_posts' => true,
			'delete_published_pages' => true,
			'delete_published_posts' => true,
			'edit_others_pages' => true,
			'edit_others_posts' => true,
			'edit_pages' => true,
			'edit_posts' => true,
			'edit_private_pages' => true,
			'edit_private_posts' => true,
			'edit_published_pages' => true,
			'edit_published_posts' => true,
			'manage_categories' => true,
			'manage_links' => true,
			'moderate_comments' => true,
			'publish_pages' => true,
			'publish_posts' => true,
			'read' => true,
			'read_private_pages' => true,
			'read_private_posts' => true,
			'upload_files' => true
		)
	);
	$result = add_role(
	    'amin-administrator',
	    __( 'Amin Administrator' ),
		array(
			'delete_others_pages' => true,
			'delete_others_posts' => true,
			'delete_pages' => true,
			'delete_posts' => true,
			'delete_private_pages' => true,
			'delete_private_posts' => true,
			'delete_published_pages' => true,
			'delete_published_posts' => true,
			'edit_others_pages' => true,
			'edit_others_posts' => true,
			'edit_pages' => true,
			'edit_posts' => true,
			'edit_private_pages' => true,
			'edit_private_posts' => true,
			'edit_published_pages' => true,
			'edit_published_posts' => true,
			'edit_theme_options' => true,
			'export' => true,
			'import' => true,
			'list_users' => true,
			'manage_categories' => true,
			'manage_links' => true,
			'manage_options' => true,
			'promote_users' => true,
			'publish_pages' => true,
			'publish_posts' => true,
			'read_private_pages' => true,
			'read_private_posts' => true,
			'read' => true,
			'remove_users' => true,
			'switch_themes' => false,
			'upload_files' => true
		)
	);
};

function role_checker(){
	if (is_user_logged_in()) {
		$current_user = wp_get_current_user();
		if($current_user->roles[0] == 'agency'){
			add_action('admin_head', 'hide_add_new_for_agency_users');
			add_action( 'admin_menu', 'remove_agency_menus' );
			add_action( 'wp_dashboard_setup', 'custom_dashboard_widgets' );
			//add_action( 'wp_dashboard_setup', 'add_staff_form' );
			if (isset($_GET['post_type']) && isset($_GET['post_type']) == 'work' || isset($_GET['post_type']) == 'post') {
				add_action('save_post', 'update_work_meta');
			}
		} else if($current_user->roles[0] == 'pr-editor') {
			add_action( 'admin_menu', 'remove_pr_menus' );
			add_action( 'wp_dashboard_setup', 'custom_dashboard_widgets' );
		} else if($current_user->roles[0] == 'amin-administrator') {
			add_action('admin_head', 'amin_administrator_hide_menus');
			if (isset($_GET['post_type']) && isset($_GET['post_type']) == 'work' || isset($_GET['post_type']) == 'post') {
				add_action('save_post', 'update_work_meta');
			}
		} else if($current_user->roles[0] == 'staff') {
			add_action('admin_head', 'hide_add_new_for_agency_users');
			add_action('admin_menu', 'remove_agency_menus');
			echo '<style>#wp-admin-bar-root-default{display:none!important;}#wp-admin-bar-search{display:none!important;}</style>';
		}
	}
}
add_action('init', 'role_checker');

function remove_agency_menus(){

	remove_menu_page( 'index.php' );                  		//Dashboard
	remove_menu_page( 'edit.php?post_type=page' );    		//Pages
	remove_menu_page( 'edit.php?post_type=event' );   		//Events
	remove_menu_page( 'edit.php?post_type=press-news');   	//Press News
	remove_menu_page( 'edit.php?post_type=press-download'); //Press Download
	remove_menu_page( 'edit.php?post_type=speaker'); 		//Speaker
	remove_menu_page( 'upload.php' );   			  		//Media
	remove_menu_page( 'edit-comments.php' );          		//Comments
	remove_menu_page( 'themes.php' );                 		//Appearance
	remove_menu_page( 'plugins.php' );                		//Plugins
	remove_menu_page( 'users.php' );                  		//Users
	remove_menu_page( 'tools.php' );                  		//Tools
	remove_menu_page( 'options-general.php' );        		//Settings

	remove_submenu_page(
		'edit.php?post_type=agency',
		'post-new.php?post_type=agency'
	);

}

function remove_pr_menus(){

	global $submenu;
	// var_dump($submenu); die();

	remove_menu_page( 'index.php' );                  		//Dashboard
	remove_menu_page( 'edit.php?post_type=page' );    		//Pages
	remove_menu_page( 'edit.php?post_type=person' );    	//People
	remove_menu_page( 'edit.php?post_type=client' );    	//Clients
	remove_menu_page( 'edit.php?post_type=work' );    		//Work
	remove_menu_page( 'upload.php' );   			  		//Media
	remove_menu_page( 'edit-comments.php' );          		//Comments
	remove_menu_page( 'themes.php' );                 		//Appearance
	remove_menu_page( 'plugins.php' );                		//Plugins
	remove_menu_page( 'users.php' );                  		//Users
	remove_menu_page( 'tools.php' );                  		//Tools
	remove_menu_page( 'options-general.php' );        		//Settings

	remove_submenu_page(
		'edit.php?post_type=agency',
		'post-new.php?post_type=agency'
	);

	unset($submenu['edit.php?post_type=press-download'][15]);
	unset($submenu['edit.php?post_type=press-news'][16]);

}

function hide_add_new_for_agency_users() {
	echo '<style>#wp-admin-bar-new-content{display:none!important;}.post-type-agency #delete-action{display:none;}.post-type-agency .tablenav{display:none;}#edit-slug-buttons{display:none;}.edit-post-status{display:none;}.edit-visibility{display:none;}.subsubsub{display:none;}.search-box{display:none;}.user-rich-editing-wrap{display:none!important;}.user-admin-color-wrap{display:none!important;}.user-comment-shortcuts-wrap{display:none!important;}.user-admin-bar-front-wrap{display:none!important;}.wrap div.error{display:none!important}.update-nag{display:none!important;}</style>';
	if (isset($_GET['post_type']) && isset($_GET['post_type']) == 'work' || isset($_GET['post_type']) == 'post') {
		echo '<script>jQuery(document).ready(function(){jQuery("#publish").val("Submit for review");});</script>';
		echo '<style>#publish{font-size:12px;}</style>';
	}
	if (isset($_GET['post_type']) && $_GET['post_type'] == 'agency') {
		echo '<style>h1{margin-bottom: 10px!important;} h1 a { display:none; }.trash{display:none;}.row-actions{position: static!important;}.row-actions .inline{display:none;}.check-column{display:none;}</style>';
	}
}

function hide_admin_bar_item(){
	echo '<style>#wp-admin-bar-new-content{display:none!important;</style>';
}

function amin_administrator_hide_menus() {
	echo '<style>#menu-posts-agency,#menu-tools,#menu-posts-agency,#menu-appearance,#menu-plugins,#menu-settings,#toplevel_page_edit-post_type-acf-field-group,#toplevel_page_seo,#toplevel_page_wordpress-https,#menu-posts-client,#sdf_dashboard_widget,.update-nag{display:none;}</style>';
}

add_filter( 'wp_mail_content_type', 'set_content_type' );
function set_content_type( $content_type ) {
	return 'text/html';
}

function update_work_meta($post_id) {

	// If this is just a revision, don't send the email.
	if (wp_is_post_revision($post_id))
		return;
	update_post_meta($post_id, 'approved', 0);
	$post_title = get_the_title($post_id);
	$post_url = get_permalink($post_id);
	$subject = 'A work asset has been updated';

	$message = "A work/case study asset has been updated on the AMIN website:\n\n";
	$message .= $post_title . ": " . $post_url . "\n\n";
	$message .= "To approve these changes, <a href='" . get_edit_post_link($post_id) . "'>click here.</a>";

	wp_mail(array('cchancellor-street@thinkingjuice.co.uk'), $subject, $message);
}
