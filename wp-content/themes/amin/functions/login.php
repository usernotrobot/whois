<?php

function custom_login_middle() {
    if (is_page('login')) {
        return "<p class='reminder-text bold'>If you have forgotten your login details, please contact your agency's community manager.</p>";
    } else {
        return false;
    }
};

add_action('login_form_middle', 'custom_login_middle');


function redirect_login_page() {
  $login_page  = home_url( '/login/' );
  $page_viewed = basename($_SERVER['REQUEST_URI']);
  //we parse the url so that we can get the path and query and match to on the path.
  $url = parse_url(basename($_SERVER['REQUEST_URI']));
  //print_r($url);

  if( $url['path'] == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
    wp_redirect($login_page);
    exit;
  }
}

//add_action('init','redirect_login_page');

/**
 * If a login fails
 */
function login_failed() {
  $login_page  = home_url( '/login/' );
  wp_redirect( $login_page . '?login=failed' );
  exit;
}

add_action( 'wp_login_failed', 'login_failed' );

/**
 * We need to verify their username and password and make sure it's not empty.
 */
function verify_username_password( $user, $username, $password ) {
  $login_page  = home_url( '/login/' );
    if( $username == "" || $password == "" ) {
        wp_redirect( $login_page . "?login=empty" );
        exit;
    }
}
add_filter( 'authenticate', 'verify_username_password', 1, 3);

/**
 * Make sure we reidrect to the login page on logout
 */
function logout_page() {
  $login_page  = home_url( '/login/' );
  wp_redirect( $login_page . "?login=false" );
  exit;
}
add_action('wp_logout','logout_page');
