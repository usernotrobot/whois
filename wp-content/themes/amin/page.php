<?php
$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
if(WP_ENV == 'production'){
    $context['base_url'] = 'https://' . $_SERVER['SERVER_NAME'];
} else {
    $context['base_url'] = 'http://' . $_SERVER['SERVER_NAME'];
}
if($post->ID == 246 ||  $post->ID == 249 || $post->ID == 252){ // The ID's of the network area posts. :)
    $context['region'] = getRegion($post->ID);
    $timberRegion = new TimberTerm($context['region']->term_id);
    $context['agencies'] = $timberRegion->posts(-1, 'agency');

    foreach ($context['agencies'] as $agency) {
        $agency->country = $agency->get_terms('country')[0];
        $bg_image = $agency->get_field('regional_background_image');
        $agency->coverImage = $bg_image;
        //$agency->coverImage = new TimberImage($agency->regional_background_image);
        // $agency->agencyLogo = extractSVG($agency->get_field('logo')['url']);
        $agencyLogo = $agency->get_field('logo');
        $agency->logo_url = $agencyLogo['url'];

    };
    //sort them by country order.
    if ($context['region']->slug != 'americas') {
        usort($context['agencies'], 'sortByCountry');
    } else {
        usort($context['agencies'], 'sortOnPostTitle');
    }
    
	$context['show_recruiting_modal'] = myCountryIsRecruiting($post->post_title);

	Timber::render(array('network-area.twig'), $context);
} else {
	Timber::render(array($post->post_name . '.twig', 'page.twig'), $context);
}
