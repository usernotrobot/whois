<?php
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$args = array(
	'post_type' => 'press-download',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'meta_query' => array(
	    array(
	        'key' => 'download',
	        'value'   => array(''),
	        'compare' => 'NOT IN'
	    )
	)
);

if(isset($_GET['type'])) {
	$tax = array(
		'tax_query' => array(
			array(
				'taxonomy' => 'press-download-type',
				'field' => 'slug',
				'terms' => $_GET['type']
			)
		)
	);
	$args = $args + $tax;
}

$context['downloads'] = Timber::get_posts($args);

$context['types'] = Timber::get_terms('press-download-type');

Timber::render('downloads.twig', $context);
