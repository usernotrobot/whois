var ko = require('knockout');
//Clients page
function clientModel() {
    var self = this;
    //Initialise obervable with non-alphabetical character to differentiate
    self.activeFilter = ko.observable('#');
    self.clients = ko.observableArray(WP.clients.sort());
    self.clientFilters = ko.observableArray();
    self.filteredClients = ko.observableArray([]);
    self.resetClients = function(data, event) {
        setTimeout(function() {
            self.clientFilters().forEach(function(filter){
                filter.isSelected(false);
            });
            self.activeFilter('#');
        }, 500);
    }
    self.setActiveFilter = function(data, event) {
        if(data.isActive) {
            self.clientFilters().forEach(function(filter){
                filter.isSelected(false);
            });
            self.activeFilter(data.name);
            data.isSelected(true);
        }
    }
    self.clientsFilter = ko.computed(function() {
        //Empties out array
        self.filteredClients([]);
        if(self.activeFilter() == '#') {
            self.filteredClients(self.clients());
        } else {
            ko.utils.arrayMap(self.clients(), function(client) {
                if(client.post_title[0] == self.activeFilter().toUpperCase()) {
                    //Array of clients to push to array
                    self.filteredClients.push(client);
                }
            });
        }
    });

    WP.client_filters.forEach(function(filter){
        self.clientFilters.push(new Filter(filter));
    });
}

function Filter(data) {
    var self = this;

    self.name = data.name;
    self.isActive = data.isActive;
    self.isSelected = ko.observable(false);
}

module.exports = clientModel;
