var ko = require('knockout');

function newsModel() {
	var self = this;

	//Variable assignment for speeds
	self.rawNews = WP.news;
	self.rawAgencies = WP.agencyFilters;

	//Are the regions or agencies dropdowns active?
	self.regionsActive = ko.observable(true);
	self.agenciesActive = ko.observable(false);

	//Active filter
	self.activeCategory = ko.observable();
	self.activeRegion = ko.observable();
	self.activeAgency = ko.observable();
    self.activeTag = ko.observable();

	//Filters
	self.newsCategories = ko.observableArray(WP.categories);
	self.regions = ko.observableArray(WP.regions);
	self.agencies = ko.observableArray(self.rawAgencies);

	//The posts array itself
	self.newsPosts = ko.observableArray(self.rawNews);

	//Page number
	self.pageNumber = ko.observable(1);
	self.postsPerPage = 12;

	//Show the regions dropdown
	self.showRegions = function(event) {
		self.regionsActive(!self.regionsActive());
	}

	self.hideRegions = function(event) {
		if(self.regionsActive() == true) {
			self.regionsActive(false);
		}
	}

	//Show the agencies dropdown
	self.showAgencies = function(data, event) {
		self.selectRegion(data, event);
		self.filteredAgencies();

		if(self.agencies().length > 0) {
			self.agenciesActive(true);
		} else {
			setTimeout(function() {
				self.regionsActive(false);
			}, 500);
		}
	}

	//Set the active category
	self.selectCategory = function(data, event) {
		$('.news-pods').addClass('hideNews');
		$('#all a').removeClass('isActive');
		self.activeCategory(data.slug);
		self.regionsActive(false);

		setTimeout(function() {
			self.filterPostsByCategory();

			setTimeout(function() {
				$('.news-pods').removeClass('hideNews');
			}, 350);
		}, 350);
	}

	//Set the active region
	self.selectRegion = function(data, event) {
		$('.news-pods').addClass('hideNews');
		self.activeRegion(data.slug);

		setTimeout(function() {
			self.filterPostsByRegion();

			setTimeout(function() {
				$('.news-pods').removeClass('hideNews');
			}, 350);
		}, 350);
	}

	//Set the active agency
	self.selectAgency = function(data, event) {
		$('.news-pods').addClass('hideNews');
		self.activeAgency(data.slug);

		setTimeout(function() {
			self.filterPostsByAgency();

			setTimeout(function() {
				$('.news-pods').removeClass('hideNews');

				setTimeout(function() {
					self.regionsActive(false);
				}, 750);
			}, 350);
		}, 350);
	}

	//Filter the agencies by region
	self.filteredAgencies = function() {
		self.agencies([]);
		ko.utils.arrayMap(self.rawAgencies, function(agency) {
			if(agency.region == self.activeRegion()) {
				self.agencies.push(agency);
			}
		});
	}

	//Calculate number of pages from posts
	self.totalPages = ko.computed(function() {
		return Math.ceil(self.newsPosts().length / self.postsPerPage);
	});

	//Actually create the array of pages
	self.pagination = ko.computed(function() {
		var list = [],
			i;
		for (i=1;i<=self.totalPages();i++) {
		    list.push({
				'index': i
			});
		}
		return list;
	});

	//Set active page
	self.setCurrentPage = function(data, event) {
		var hash = '#page' + data.index;
		var pos = $('.news-pods').offset();
		self.pageNumber(data.index);
		history.pushState(null, null, hash);
		$('html, body').animate({
			scrollTop: (pos.top - 130)
		}, 350);
	}

    self.paginate = function(direction) {
		var pos = $('.news-pods').offset();
        if (direction == 'next') {
            var newPageNum = self.pageNumber() + 1;
			history.pushState(null, null, hash);
        } else if (direction == 'prev') {
            var newPageNum = self.pageNumber() - 1;
        }
		var hash = '#page' + newPageNum;
		self.pageNumber(newPageNum);
		history.pushState(null, null, hash);
		$('html, body').animate({
			scrollTop: (pos.top - 130)
		}, 350);
    }

	//Paginates results & chunks array into rows
	self.pagedRows = ko.computed(function() {
		var rows = [],
			pages = [],
			numPages = self.totalPages(),
			first = (self.pageNumber() - 1) * self.postsPerPage,
			paged = self.newsPosts.slice(first, first + self.postsPerPage);

			for(var i=0;i<paged.length;i++) {
		        var row = paged[i];
		        if(i % 3 === 0) {
		            rows.push([]);
		        }
		        rows[Math.floor(i/3)].push(row);
		    }

		    return rows;
	});

	//Filters posts by region
	self.filterPostsByRegion = function() {
		self.newsPosts([]);
		self.pageNumber(1);
		ko.utils.arrayMap(self.rawNews, function(post) {
			if(post.region == self.activeRegion()) {
				self.newsPosts.push(post);
			}
		});
	}

	//Filters posts by agency
	self.filterPostsByAgency = function() {
		self.newsPosts([]);
		self.pageNumber(1);
		ko.utils.arrayMap(self.rawNews, function(post) {
			if(post.agency == self.activeAgency()) {
				self.newsPosts.push(post);
			}
		});
	}

	//Filters posts by category
	self.filterPostsByCategory = function() {
		self.newsPosts([]);
		self.pageNumber(1);
		console.log(self.activeCategory());
		ko.utils.arrayMap(self.rawNews, function(post) {
			if(post.categories.indexOf(self.activeCategory()) > -1) {
				self.newsPosts.push(post);
			}
		});
	}

    //Filter posts by tags
    self.filterPostsByTag = function() {
        self.newsPosts([]);
        self.pageNumber(1);
        ko.utils.arrayMap(self.rawNews, function(post){
            if (post.tags.indexOf(self.activeTag()) != -1) {
                console.log(true);
                self.newsPosts.push(post);
            }
        });
    }

	//Show all posts
	self.resetPosts = function() {
		$('.news-pods').addClass('hideNews');
		$('.region a').removeClass('isActive');
		self.regionsActive(false);
		self.agenciesActive(false);
		self.activeRegion();

		setTimeout(function() {
			self.newsPosts(self.rawNews);

			setTimeout(function() {
				$('.news-pods').removeClass('hideNews');
			}, 350);
		}, 350);
	}

    //Decode entities
    self.decodeEntities = function(str) {
        //This prevents any overhead from creating the object each time
        var element = document.createElement('div');

        if(str && typeof str === 'string') {
          //Strip script/html tags
          str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
          str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
          element.innerHTML = str;
          str = element.textContent;
          element.textContent = '';
        }

        return str;
    }

    self.getHash = function() {
        var hash = window.location.hash;

        if (hash.length > 0) {
			hash = hash.replace('#', '').toLowerCase();
			if(hash.indexOf('page') > -1) {
				var pageNum = parseInt(hash.replace('page', ''));
				self.pageNumber(pageNum);
			} else if(hash.indexOf('cat') > -1) {
				var cat = hash.replace('cat-', '');
				var data = {
					slug: cat
				};
				self.selectCategory(data, null);
			} else {
				self.activeTag(hash);
	            self.filterPostsByTag();
			}
        } else {
            //Do nothing, no need
        }
    }
}

module.exports = newsModel;
