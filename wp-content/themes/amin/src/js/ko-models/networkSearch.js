var ko = require('knockout');
var Fuse = require('fuse.js');

function networkSearchModel(people, agencies, countries) {
    var self = this;

    self.searchTerm = ko.observable('');
    self.peopleData = ko.observableArray(people);

    self.currentPersonHero = ko.observable('');
    self.currentPersonAgencyLogo = ko.observable('');
    self.currentPersonName = ko.observable('');
    self.currentPersonRole = ko.observable('');
    self.currentPersonPhone = ko.observable('');
    self.currentPersonEmail = ko.observable('');
	self.currentPersonRelated = ko.observableArray([]);

	self.isHovering = ko.observable(false);

    self.peopleSearch = new Fuse(people, {
        caseSensitive: false,
        shouldSort: true,
        threshold: 0.4,
        location: 0,
        distance: 50,
        maxPatternLength: 32,
        keys: ['name', 'position', 'position_group']
    });
    self.agencySearch = new Fuse(agencies, {
        caseSensitive: false,
        shouldSort: true,
        threshold: 0.4,
        location: 0,
        distance: 50,
        maxPatternLength: 32,
        keys: ['name']
    });
    self.countriesSearch = new Fuse(countries, {
        caseSensitive: false,
        shouldSort: true,
        threshold: 0.4,
        location: 0,
        distance: 50,
        maxPatternLength: 32,
        keys: ['name']
    });

    self.peopleResults = ko.computed(function() {
		self.currentPersonName('');
        self.currentPersonRole('');
		self.currentPersonHero('');
		self.currentPersonAgencyLogo('');
		self.currentPersonPhone('');
		self.currentPersonEmail('');
		self.currentPersonRelated([]);

        if (self.searchTerm().length > 2) {
			return self.peopleSearch.search(self.searchTerm()).splice(0, 10);
        } else {
            //do nothing
            return [];
        }
    });

    self.agencyResults = ko.computed(function() {
        if (self.searchTerm().length > 2) {
            return self.agencySearch.search(self.searchTerm()).splice(0, 10);
        } else {
            //do nothing
            return [];
        }
    });

    self.countryResults = ko.computed(function() {
        if (self.searchTerm().length > 2) {
            return self.countriesSearch.search(self.searchTerm()).splice(0, 10);
        } else {
            //do nothing
            return [];
        }
    });

    self.changePersonImage = function(person, e) {
        self.currentPersonName(person.name);
        self.currentPersonRole(person.position);
		self.currentPersonPhone(person.phone);
		self.currentPersonEmail(person.email);
		self.currentPersonRelated(person.related);

		self.isHovering(true);

        if (person.hero_image) {
            self.currentPersonHero(person.hero_image.url);
        } else {
            self.currentPersonHero('');
        }

        if (person.agency_logo) {
            self.currentPersonAgencyLogo(person.agency_logo.url);
        } else {
            self.currentPersonAgencyLogo('');
        }
    };
}

module.exports = networkSearchModel;
