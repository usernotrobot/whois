jQuery(document).ready(function() {
	// Provide the class cc-mod on the field in ACF, and put the numerical value of the limit as a placeholder.
	if(jQuery('.cc-mod').length > 0) {
		var parents = jQuery('.cc-mod');
		parents.each(function() {
			var input = jQuery(this).find('input, textarea');
			var label = jQuery(this).find('.acf-label');
			var limit = jQuery(input).attr('placeholder');
			var count = jQuery(input).val().length;
			jQuery(input).removeAttr('placeholder');
			jQuery(label).append('<p>Characters remaining: <span class=\'charCounter\'></span></p>');
			var counter = jQuery(this).find('.charCounter');
			var remaining = limit - count;
			jQuery(counter).text(remaining);
			jQuery(input).on('keyup', function(e) {
				var count = jQuery(this).val().length;
				var remaining = limit - count;
				jQuery(counter).text(remaining);
			});
		});
	}
});