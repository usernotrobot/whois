/*
* A slider module that will allow you to slide two halfs of a div
* in different directions on scroll.
* Uses jquery as a passed in dependancy
*/

function Elliptical(options) {
    var self = this;
    var $ = {};

    if (options.jQuery) {
        $ = options.jQuery;
    } else {
        $ = window.$;
    }

    self.container = wrapObj(options.container);
    self.left = self.container.find(options.leftHalf);
    self.right = self.container.find(options.rightHalf);
    self.itemSelector = self.container.find(options.itemSelector);
    self.loop = options.loop || false,
    self.scrollJack = options.scrollJack || false,
    self.dots = self.container.find(options.dots),
    self.itemCount = self.itemSelector.length / 2; // divide it again because it will find double the amount because of both halves
    self.itemHeight = 100/self.itemCount;
    self.index = 0;
    self.quietPeriod = 500;
    self.transition = options.transitionSpeed || 350;
    self.lastAnimation = 0;
    self.lockedIn = false;
    self.scrollDirection ='';


    self.goTo = function(index) {
        if (index < 0 || index > self.itemCount - 1) {
            if (self.loop) {
                index = 0;
            } else {
                return;
            }
        }

        self.right.css('transform', 'translate3d(0,' + self.calculateOffset('down', index) + '%,0)');
        self.left.css('transform', 'translate3d(0,' + self.calculateOffset('up', index) + '%,0)');

        self.index = index;
        self.updateDom();
    }

    self.next = function() {
        self.goTo(self.index+1);
    }

    self.prev = function() {
        self.goTo(self.index-1);
    }

    self.calculateOffset = function(direction, index) {
        if (direction == 'down') {
            //when going down the index needs to be 1 more step because of how translating works
            index++;
            return (100 - (self.itemHeight * index)) * -1;
        } else if (direction == 'up') {
            return (self.itemHeight * index) * -1;
        } else {

        }
    }

    self.activateScroll = function(bool) {
        if (bool) {
            var $window = $(window);
            var windowBottom = $window.height();
            var containerBottom = self.container.height();

            $window.on('scroll mousewheel', function(e){
                var delta = e.originalEvent.wheelDelta;

                if (delta < 0) {
                    self.scrollDirection = 'down';
                } else {
                    self.scrollDirection = 'up';
                }

                // if ($window.scrollTop() >= self.container.offset().top && $window.scrollTop() <= self.container.offset().top + 10) {
                //     self.lockedIn = true;
                //     self.handleScroll(e, delta);
                // }

                //logic for scrolling down
                if ($window.scrollTop() + 80 >= self.container.offset().top && self.lockedIn == false && self.scrollDirection == 'down' && self.index != self.itemCount - 1) {
                    self.lockedIn = true;
                } else if ($window.scrollTop() + 80 >= self.container.offset().top && self.lockedIn == true && self.scrollDirection == 'down') {

                    if (self.scrollDirection == 'down' && self.index == self.itemCount - 1) {
                        return true;
                    } else {
                        self.handleScroll(e);
                    }
                }

                //logic for scrolling back up
                if ($window.scrollTop() + 80 <= self.container.offset().top && self.lockedIn == false && self.scrollDirection == 'up' && self.index != 0) {
                    self.lockedIn = true;
                } else if ($window.scrollTop() + 80 <= self.container.offset().top && self.lockedIn == true && self.scrollDirection == 'up') {

                    if (self.scrollDirection == 'up' && self.index == 0) {
                        return true;
                    } else {
                        self.handleScroll(e);
                    }
                }
            })
        }
    }

    self.deActivateScroll = function() {
        var $window = $(window);
        $window.off('scroll mousewheel');
    }

    self.handleScroll = function(e) {
        //var delta = e.originalEvent.wheelDelta; // remember the magic mouse is set to reverse so positive is down and up is minus
        var now = new Date().getTime();

        if (self.lockedIn) {
            if (now - self.lastAnimation < self.quietPeriod + self.transition) {
                e.preventDefault();
                return;
            }

            if (self.scrollDirection == 'down') {
                self.next();
            } else {
                self.prev();
            }

            self.lastAnimation = now;
        }
    }

    self.createDots = function() {
        var ul = $('<ul></ul>');

        for (var i = 0, l = self.itemCount; i < l; i++) {
            var li = '<li data-index="' + i + '"></li>';
            ul.append(li);
        }

        self.dots.append(ul);
        self.dotsLi = self.dots.find('li');
        self.bindDots();
    }

    self.bindDots = function() {
        self.dotsLi.on('click', function(e){
            var $this = $(this);
            var index = $this.attr('data-index');

            self.goTo(index);
        });
    }

    //loop through all the items and the dots and then check the index and map it to the slides index, then add an active class
    self.updateDom = function() {
        //loop through the items
        self.itemSelector.each(function(i, el){
            var $el = $(el);

            if ($el.attr('data-index') == self.index) {
                $el.addClass('active');
            } else {
                $el.removeClass('active');
            }
        });

        self.dotsLi.each(function(i,el){
            var $el = $(el);

            if ($el.attr('data-index') == self.index) {
                $el.addClass('active');
            } else {
                $el.removeClass('active');
            }
        })
    }

    //do some maths to set the right heights based on the amount of items
    self.left.css('height', 100 * self.itemCount + '%');
    self.right.css('height', 100 * self.itemCount + '%');
    self.itemSelector.css('height', 100 / self.itemCount + '%');

    //we need to translate them their height based on the left and right.
    self.right.css('transform', 'translate3d(0,' + self.calculateOffset('down', self.index) + '%,0)');
    self.left.css('transform', 'translate3d(0,' + self.calculateOffset('up', self.index) + '%,0)');
    //lets all set the transition period to be the same as our transition set in the options
    self.right.css('transition-duration', self.transition / 1000 + 's');
    self.left.css('transition-duration', self.transition / 1000 + 's');

    self.createDots();
    //do we want to scroll Jack
    self.activateScroll(self.scrollJack);

    self.updateDom();

}

/*
* Check if it's a jquery object.
* If not return it wrapped in jquery
*/
function wrapObj(obj) {
    if (obj instanceof jQuery) {
        return obj;
    } else {
        return $(obj);
    }
}

module.exports = Elliptical;
