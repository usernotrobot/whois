/*
* This module expects a a mapbox map instance and will then get it to generate and add the layers as needed.
*/

var blue = '#126a9c';

module.exports = {
    activeRegion: '',

    addRecruitingMarkers: function(map, radius) {
        map.addLayer({
            'id': "RecruitingMarkersCircles",
            'type': 'circle',
            'interactive': true,
            'source': 'recruitingMarkers',
            'paint': {
                'circle-radius': radius,
                'circle-color': '#444',
                'circle-opacity': .5
            },
            'filter': ['==', 'region', '']
        });

        //Active marker layers
        map.addLayer({
            'id': 'RecrutingMarkersCirclesOuter',
            'type': 'circle',
            'interactive': true,
            'source': 'recruitingMarkers',
            'paint': {
                'circle-color': '#333',
                'circle-radius': (radius*3),
                'circle-opacity': .3
            },
            'filter': ['==', 'title', '']
        });

        map.addLayer({
            'id': 'RecruitingMarkersCirclesOuterInner',
            //'ref': 'RecrutingMarkersCirclesOuter',
            'type': 'circle',
            'interactive': true,
            'source': 'recruitingMarkers',
            'paint': {
                'circle-color': '#333',
                'circle-radius': (radius*2),
                'circle-opacity': .5
            },
            'filter': ['==', 'title', '']
        });

        //Hover layers, only shown if you're hovering over the correct layer
        map.addLayer({
            'id': 'RecruitingMarkersCirclesHover',
            'type': 'circle',
            'interactive': true,
            'source': 'recruitingMarkers',
            'paint': {
                'circle-color': '#333',
                'circle-radius': radius,
                'circle-opacity': .8
            },
            'filter': ['==', 'title', '']
        });

        //extra layers of active state
        map.addLayer({
            'id': 'RecruitingMarkersCirclesActive',
            'type': 'circle',
            'interactive': true,
            'source': 'recruitingMarkers',
            'paint': {
                'circle-color': '#333',
                'circle-radius': radius
            },
            'filter': ['==', 'title', '']
        });
    },
    /*
    * Pass in the map you want to do some switching and jquery to do some dom selecting
    */
    handleMapSwitch: function(map, $) {
        var tabs = $('.map-toggles .tab-text');

        var $firstTab = $('.tab-text.isActive');
        // We're also do this when the function is first called
        var filter = $firstTab.attr('data-filter');
        var mapCenter = $firstTab.attr('data-center').split(',').map(function(item){ return parseFloat(item); });

        module.exports.activeRegion = {
            filter: filter,
            mapCenter: mapCenter
        };

        tabs.on('click', function(e){
            e.preventDefault();
            $this = $(this);
            var filter = $this.attr('data-filter');
            var mapCenter = $this.attr('data-center').split(',').map(function(item){ return parseFloat(item); });

            module.exports.activeRegion = {
                filter: filter,
                mapCenter: mapCenter
            };

            //reveal recruting markers
            map.setFilter('RecruitingMarkersCircles', ['==', 'region', filter]);

            map.flyTo({
                center: mapCenter
            });

        });

        //reveal recruting markers
        map.setFilter('RecruitingMarkersCircles', ['==', 'region', 'europe-africa-the-middle-east']);
    },

    setUpClicks: function(map, popup, popupBuilder, $) {
        map.on('click', function(e){

            var features = map.queryRenderedFeatures(e.point, {
                layers: ['RecruitingMarkersCircles']
            });

            if (!features.length > 0) {
                //Do nothing
            } else {
                var feature = features[0];

                popup.setLngLat(feature.geometry.coordinates)
                .setHTML(popupBuilder(feature))
                .addTo(map);

                //attach an event listener for the cross in popup
                $('.mapboxgl-popup-content .close-icon, .js-zoom-out').on('click', function(e){
                    module.exports.closeAndZoomOut(map, popup, module.exports.activeRegion.mapCenter);
                });

                if (map.getZoom() > 6) {
                    var zoom = map.getZoom();
                } else {
                    var zoom = 6;
                }

                map.flyTo({
                    center: feature.geometry.coordinates,
                    zoom: zoom,
                    pitch: 20
                });

                //set the marker layer AgencyMarkersCirclesOuter
                map.setFilter('RecrutingMarkersCirclesOuter', ['==', 'title', feature.properties.title]);
                map.setFilter('RecruitingMarkersCirclesOuterInner', ['==', 'title', feature.properties.title]);
                map.setFilter('RecruitingMarkersCirclesActive', ['==', 'title', feature.properties.title]);
            }
        });
    },

    setUpMouseMoves: function(map) {
        map.on('mousemove', function(e){

            var features = map.queryRenderedFeatures(e.point, {
                layers: ['RecruitingMarkersCircles']
            });

            map.getCanvas().style.cursor = features.length > 0 ? 'pointer' : '';

            if (!features.length > 0) {
                map.setFilter('RecruitingMarkersCirclesHover', ['==', 'title', '']);
            } else {
                var feature = features[0];

                if (feature) {
                    map.setFilter('RecruitingMarkersCirclesHover', ['==', 'title', feature.properties.title]);
                } else {
                    map.setFilter('RecruitingMarkersCirclesHover', ['==', 'title', '']);
                }
            }
        });
    },

    closeAndZoomOut: function(map,popup, center) {
        popup.remove();
        map.flyTo({
            zoom: 2.5,
            pitch: 0,
            center: center
        });
        activeMarker = false;
        map.setFilter('RecrutingMarkersCirclesOuter', ['==', 'title', '']);
        map.setFilter('RecruitingMarkersCirclesOuterInner', ['==', 'title', '']);
        map.setFilter('RecruitingMarkersCirclesActive', ['==', 'title', '']);
    },

    getRegionDetails: function(region) {
        var blue = '#126a9c';
        var orange = '#f38e4e';
        var green = '#00BC98';

        switch(region) {
            case 'americas':
                return {
                    'color': blue,
                    'class': 'blue',
                    'center': [-75.01387886338617, 16.72824651667497]
                };
                break;
            case 'europe-africa-the-middle-east':
                return {
                    'color': orange,
                    'class': 'orange',
                    'center': [29.62426207110748, 30.578755210404296]
                };
                break;
            case 'asia-pacific':
                return {
                    'color': green,
                    'class': 'green',
                    'center': [135.38434570244647, 4.307642285149967]
                };
                break;
            default:
                return {
                    'color': orange,
                    'class': 'orange',
                    'center': [29.62426207110748, 30.578755210404296]
                };
                break;
        }
    }
}
