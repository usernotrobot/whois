function menuScroll() {
    var bannerBar = $('.banner-bar');
	var subBar = $('.filters-sub-bar');
    //'scroll-header' is class that must be applied to top hero image. Takes into account variance in height.
    var scrollHeader = $('.scroll-header');
    var header = $('.top-nav');
    var headerHeight = header.height();
    var windowWidth = $(window).width();
    var scrollDistance = scrollHeader.innerHeight() - headerHeight;

    //Collect all the scroll points of this page by a js-scrollPoint class
    var $scrollPoints = $('.js-scrollPoint');
    var nav = $('.js-scrollPoint-nav');
    var scrollData;
    //we work out the distnaces between points by waiting until everything has loaded
    $(window).on('load', function(){
        scrollData = extractScrollPoints($scrollPoints);

		console.log(scrollData);

		if(windowWidth >= 768) {
	        window.onscroll = function(e) {
	            var scrollPos = window.scrollY;
	            var classNames = bannerBar.classNames;
	            if(scrollPos >= scrollDistance) {
	                bannerBar.addClass('fixed');
	                subBar.addClass('fixed');
	            } else {
	                bannerBar.removeClass('fixed');
	                subBar.removeClass('fixed');
	            }
	            if (typeof scrollData != 'undefined' || scrollData != '' || scrollData.length > 0) {
	                checkScrollPoints(scrollData, scrollPos, nav);
	            }
	        }
	    }
    });
}

/*
* This takes a jquery collection of dom elements you want to
* work out the scroll data for, it will then return an array of objects which you can use.
*
*/
function extractScrollPoints(scrollPoints) {
    var points = [];

    scrollPoints.each(function(i, el){
        var $el = $(el);
        var startPoint;
        var endPoint;

        if (i != scrollPoints.length -1) {
            startPoint = $el.offset().top;
            endPoint = scrollPoints.eq(i + 1).offset().top;
        } else {
            startPoint = $el.offset().top;
            endPoint = $('.footer').offset().top + $('.footer').innerHeight();
        }

        points.push({
            startPoint: startPoint,
            endPoint: endPoint,
            id: $el.attr('id')
        })
    });

    return points;
}

//To be run on everyscroll event. Just call this function inside the on scroll callback.
function checkScrollPoints(scrollData, scrollY, nav) {
	if(scrollData) {
		scrollData.forEach(function(scrollPoint, i) {
	        //console.log("scrollY: %s", scrollY);
	        //console.log("startPoint: %s", scrollPoint.startPoint);
	        if (scrollY + 46 >= scrollPoint.startPoint && scrollY + 46 <= scrollPoint.endPoint) {
	            //console.log(scrollPoint.id + " should be active");
	            $el = nav.find("a[href='#" + scrollPoint.id + "']");
	            $el.parent().siblings().removeClass('isActive');
	            $el.parent().addClass('isActive');
	        }
	        // if (i == 0) {
	        //     if (scrollY + 46 >= scrollPoint.startPoint && scrollY + 46 <= scrollPoint.endPoint) {
	        //         console.log(scrollPoint.id + " should be active");
	        //         $el = nav.find("a[href='#" + scrollPoint.id + "']");
	        //         $el.parent().siblings().removeClass('isActive');
	        //         $el.parent().addClass('isActive');
	        //         //$el.addClass('isActive');
	        //     }
	        // } else if (i == scrollData.length -1 ) {
	        //     if (scrollY + 46 >= scrollData[i-1].endPoint && scrollY + 46 <= scrollPoint.endPoint) {
	        //         console.log(scrollPoint.id + " should be active");
	        //         $el = nav.find("a[href='#" + scrollPoint.id + "']");
	        //         $el.parent().siblings().removeClass('isActive');
	        //         $el.parent().addClass('isActive');
	        //         //$el.addClass('isActive');
	        //     }
	        // } else {
	        //     if (scrollY + 46 >= scrollData[i - 1].endPoint && scrollY + 46 <= scrollData[i + 1].startPoint) {
	        //         console.log(scrollPoint.id + " should be active");
	        //         $el = nav.find("a[href='#" + scrollPoint.id + "']");
	        //         $el.parent().siblings().removeClass('isActive');
	        //         $el.parent().addClass('isActive');
	        //         //$el.addClass('isActive');
	        //     }
	        // }
	    });
	}
}

module.exports = menuScroll();
