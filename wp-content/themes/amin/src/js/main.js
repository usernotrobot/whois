$(document).ready(function() {

	var ko = require('knockout');
    var mapMaker = require('./mapMaker.js');
    var memberMapMaker = require('./memberMapMaker.js');
    var Elliptical = require('./elliptical.js');
    var clientModel = require('./ko-models/clientFilter.js');
    var newsModel = require('./ko-models/newsFilter.js');
    var networkSearchModel = require('./ko-models/networkSearch.js');
    var menuScroll = require('./menuScroll.js');
    var matchHeight = require('./matchHeight.js');

    // var slick = require('slick-carousel');
    // var $ = require('jQuery');

    var mobileWidth = 768;
    $('.workslider').slick({
        accessibility: false,
        dots: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        touchMove: true,
        useTransform: true,
        centerMode: true,
        responsive: [{
            }, {
            breakpoint: mobileWidth,
            settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
        }]
    });

    //Define middle and grey slides
    if($(window).width() > mobileWidth && $('.slick-track').children().length === 1) {
      $(".slick-track, .slick-active").addClass('oneSlide');
      if ($(window).width() > mobileWidth) {
        $(".slick-track").prepend("<div class='slide slick-slide slick-active oneSlide blank'></div>");
        $(".slick-track").append("<div class='slide slick-slide slick-active oneSlide blank'></div>");
      }
    } else if($(window).width() > mobileWidth && $('.slick-track').children().length === 2) {
      $(".slick-track, .slick-active").addClass('twoSlides');
      if ($(window).width() > mobileWidth) {
        $(".slick-track").append("<div class='slide slick-slide slick-active twoSlides blank'></div>");
      }
      $(".twoSlides:eq(1), .twoSlides:eq(2)").addClass("borderRight");
      $(".twoSlides:eq(3)").css("borderRight", "4px solid black");
    } else if ($(window).width() > mobileWidth && $('.slick-track').children().length === 3) {
      $('.linked-products').css("display", "block");
      $(".slick-active:eq(0), .slick-active:eq(1)").addClass('borderRight');
      // $(".minimiseIcon").addClass("hide");
    } else if ($(window).width() > mobileWidth && $(".slick-track").children().length > 3) {
      $(".slick-active:eq(0), .slick-active:eq(2)").addClass('grey');
      $(".slick-active:eq(1)").addClass('middle');
      var currentTitle = $(".slick-active.middle").find('.img-title').text();
      $("#slide-title").text(currentTitle);

      $(".slick-prev, .slick-next").on('click', function() {
        $(".slick-active").removeClass('middle');
        $(".slick-active:eq(1)").addClass('middle');
        $(".slick-active").removeClass('grey');
        $(".slick-active:eq(0), .slick-active:eq(2)").addClass('grey');
        var currentTitle = $(".slick-active.middle").find('.img-title').text();
        $("#slide-title").text(currentTitle);
        var currentSlideRef = $(".slick-active:eq(1)").attr("data-id");
      });
    };

    //Analytics for gravity forms
    $(document).on('gform_confirmation_loaded', function (e, form_id) {
        if(form_id == 3) {
            //Become a member form
            dataLayer.push({'event': 'formsuccess', 'type': 'becameamember'});
        } else if(form_id == 1) {
            //Newsletter
            dataLayer.push({'event': 'formsuccess', 'type': 'email'});
        } else if(form_id == 2 || form_id == 4) {
            var select = $('.form-selector').val();
            var topic;
            if(select == 2) {
                topic = 'Agency Membership Enquiry';
            } else if(select == 4) {
                topic = 'New Business Enquiry';
            }
            console.log(topic);
            dataLayer.push({'event': 'formsuccess', 'type': topic});
        }
    });

	//Work & Home pages
	var iso = $('.isotope-wrapper').isotope({
        itemSelector: '.isotope-post',
        layoutMode: 'masonry',
        masonry: {
            gutter: 45,
            columnWidth: '.isotope-post'
        }
    });

	//Couldn't re-use isotope-wrapper, too specific - TW
	var principles = $('.principles-isotope').isotope({
		itemSelector: '.isotope-principle',
        layoutMode: 'masonry',
        masonry: {
            gutter: 20,
            columnWidth: '.isotope-principle'
        }
	});

	$('.isotope-filter').click(function(e) {
        $('.isotope-filter a, #all a').removeClass('isActive');
        $(this).find('a').addClass('isActive');
        var id = $(this).attr('id');
		var ctas = $('.relevant-ctas');
        if(id == 'all') {
            iso.isotope({ filter: '*' })
            principles.isotope({ filter: '*' })
        } else {
            iso.isotope({ filter: '.' + id })
            principles.isotope({ filter: '.' + id })
        }
		if(ctas.length > 0) {
			ctas.find('li').removeClass('isActive');
			ctas.find('li.' + id).addClass('isActive');
		}
    });

	//

    $('.header-text-wrapper .mute-icon').click(function() {
        $(this).removeClass('active');
        $('.header-text-wrapper .muted-icon').addClass('active');
    });

    $('.header-text-wrapper .muted-icon').click(function() {
        $(this).removeClass('active');
        $('.header-text-wrapper .mute-icon').addClass('active');
    });

	//

    if($('body').hasClass('home')) {
        var s = skrollr.init({
            mobileCheck: function() {
                //hack - forces mobile version to be off
                return false;
            },
			forceHeight: false
        });
    }

	//

	$(function() {
		$('a[href*="#"]:not([href="#"])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html, body').animate({
                        //-80px to account for fixed header
						scrollTop: (target.offset().top - 80)
					}, 1000);
					return false;
				}
			}
		});
	});

    /*
    * This is a function that will get the height of an element that needs to transition open
    * maybe an accordion or submenu. sets it as a data-attr and then you can just use it from there.
    * It looks for a class called .js-dyna-height
    */
    dynaHeight();

	$('.arrow-down').click(function(e) {
        var $this = $(this);
        var menu = $this.siblings('.sub-menu');

        if (!menu.hasClass('isActive')) {
            menu.addClass('isActive');
            menu.css('height', menu.attr('data-height'));
        } else {
            menu.removeClass('isActive');
            menu.css('height', '');
        }
	});

    $('.mobile-icon').click(function() {
        $('.mobile-navigation').toggleClass('isActive');
    });

    $('.menu-item-has-children').click(function() {
        $('.children', this).toggleClass('isActive');
        $(this).parent('ul.full').toggleClass('child-triggered');
    });

    $('.filter-btn').click(function() {
        //revisit this, this opens all filters when we only want to open the current one.
        //$('.work-filters').toggleClass('isActive');
        $(this).parents('.work-filters').toggleClass('isActive');
    });

    $('.form-selector').change(function () {
        var formid =$ (this).val();
        $('.gform_wrapper').removeClass('isActive');
        $('#gform_wrapper_' + formid).toggleClass('isActive');
        var h = $('.dynamic-forms').height() + 60;
        $('.contact-wrapper').height(h);
        if(formid == ''){
            window.location.replace("/members/#join-form");
        }
    });

	var tabText = $('.tab-text');
	var bulletsTab = $('.bullets-tab');

	tabText.click(function(e) {
		tabText.removeClass('isActive');
		bulletsTab.removeClass('isActive');
		var state = $(this).attr('id');
		$(this).addClass('isActive');
		$('.bullets-tab[id="' + state + '"]').addClass('isActive');
	});

    //

    /* Hamburger icon animation */
    $(document).ready(function(){
        $('.hamburger').click(function(){
            $(this).toggleClass('open');
        });
    });

    //
    var agencyMapView = $('.agency-map');
    var mapViewer = $('.view-switcher .map');
    var listViewer = $('.view-switcher .list');
    var switcher = $('.switcher');
    var agencyListView = $('.agency-list-view');
    var agencyList = $('.agency-list');
	var listHeight;

    var mapHeight = agencyMapView.innerHeight();
    agencyMapView.attr('data-height', mapHeight);

    agencyListView.css({ 'height':'auto'});

	function resizeMapWindow() {
		if($(window).width() > 768) {
			listHeight = agencyList.innerHeight() + 160;
			agencyListView.css({ 'height':'100%'});
			agencyMapView.css('height', listHeight);
		} else {
			listHeight = agencyList.innerHeight() + 160;
			agencyListView.css('height', listHeight);
			agencyMapView.css('height', listHeight);
		}
	}
	resizeMapWindow();
	$(window).resize(function() { resizeMapWindow(); });


    mapViewer.on('click', function(){
        var $this = $(this);
        switcher.removeClass('isRight');
        switcher.addClass('isLeft');

        listViewer.removeClass('active');
        $this.addClass('active');
        agencyListView.addClass('hidden');
        $('.recruiting-text').addClass('reveal');
        agencyMapView.css('height', mapHeight);
    });

    listViewer.on('click', function(){
        var $this = $(this);
        switcher.removeClass('isLeft');
        switcher.addClass('isRight');

        mapViewer.removeClass('active');
        $this.addClass('active');
        agencyListView.removeClass('hidden');

		listHeight = agencyList.innerHeight() + 160;
        agencyListView.css('height', listHeight);
        agencyMapView.css('height', listHeight);
    });

    switcher.on('click', function(){
        var $this = $(this);

        if ($this.hasClass('isLeft')) {
            listViewer.addClass('active');
            mapViewer.removeClass('active');

            $this.removeClass('isLeft');
            $this.addClass('isRight');

            agencyListView.removeClass('hidden');

            listHeight = agencyList.innerHeight() + 160;
            agencyListView.css('height', listHeight);
            agencyMapView.css('height', listHeight);
        } else {
            listViewer.removeClass('active');
            mapViewer.addClass('active');

            $this.removeClass('isRight');
            $this.addClass('isLeft');

            agencyListView.addClass('hidden');
            agencyMapView.css('height', mapHeight);
        }
    });

    //if there is a split slider then init a new one
    var $splitSlider = $('.split-slider');

    if ($splitSlider.length > 0) {
        splitSlider = new Elliptical({
            container: $splitSlider,
            leftHalf: '.left-half',
            rightHalf: '.right-half',
            dots: '.split-slider-dots',
            itemSelector: '.item',
            scrollJack: true,
            transitionSpeed: 500,
            loop: false
        });

        var splitSliderTriggers = $('.js-split-slider-link');

        var screenWidth = $(window).width();

        if (screenWidth >= 768 ) { // 768+ way of handling things
            var sliderTop = $('#split-slider').offset().top;
            splitSliderTriggers.on('click', function(e){
                e.preventDefault();
                //splitSlider.deActivateScroll();

                var index = parseInt($(this).attr('data-slide'));

                if (typeof index != 'undefined') {
                    $('body').animate({scrollTop: (sliderTop - 80)}, '500', function(){
                        splitSlider.goTo(index);
                        //we delay it so that the scroll logic doesn't inteerfere too quickly
                        // setTimeout(function(){
                        //     splitSlider.activateScroll(true);
                        // }, 350);
                    });
                }
            });
        } else {
            splitSliderTriggers.on('click', function(e){
                e.preventDefault();

                var index = parseInt($(this).attr('data-slide'));
                var href = $(this).attr('href');

                if (href != '#') {
                    var $dest = $(href);
                    $('body').animate({scrollTop: ($dest.offset().top - 80)}, '500', function(){
                        //do something else perhaps
                    });
                }

                if (typeof index != 'undefined' || index != 'NaN') {
                    $('body').animate({scrollTop: ($('#' + index).offset().top - 80)}, '500', function(){
                        //do something else perhaps
                    });
                }

            });
        }
    }
 
    if (typeof(WP) !== 'undefined') {
        if(WP.wholeWorld) {
            
            mapboxgl.accessToken = 'pk.eyJ1IjoiYmlyZHlib3kxOCIsImEiOiJjaWtoZGY0a3gwMDI3d2ptNGZ0ejNoNnRnIn0.RyfqOzDQmlDclZhHHjd38g';
            
            var bounds = [
                [-169, -58],
                [191.3, 75.6]
            ];

            var agencyMap = new mapboxgl.Map({
                container: 'agency-map',
                //style: 'mapbox://styles/birdyboy18/cikzcg49k000z9klziviggy2s',
                style: 'mapbox://styles/mapbox/light-v9',
                center: [10.938058021509107, 45.32604729489731], // starting position
                zoom: 1.4, // starting zoom
                pitch: 0,
                bearing: 0,
                scrollZoom: false,
                minZoom: 1.2,
                maxBounds: bounds
            });

            var popup = new mapboxgl.Popup({
                closeButton: false,
                anchor: 'right',
                closeOnClick: false
            });

            agencyMap.on('load', function(){

                agencyMap.addControl(new mapboxgl.NavigationControl(), 'top-left');
                agencyMap.doubleClickZoom.enable();

                for (i = 0; i < WP.wholeWorld.length; i++) {
                    var regionDetails = mapMaker.getRegionDetails(WP.wholeWorld[i].region);
//                    $('#agency-map').addClass(regionDetails.class);

                    var source = 'agencyMarkers-' + WP.wholeWorld[i].region; 
                    agencyMap.addSource(source, {
                        'type':'geojson',
                        'data': WP.wholeWorld[i].agencies
                    });
                    mapMaker.addAgencyMarkers(agencyMap, regionDetails, source, i, WP.wholeWorld[i].region);

                    source = 'recruitingMarkers-' + WP.wholeWorld[i].region;
                    agencyMap.addSource(source, {
                        'type':'geojson',
                        'data': WP.wholeWorld[i].recruitingCountries
                    }); 
                    mapMaker.addRecruitingMarkers(agencyMap, 10, source, i);
                    //this sets up the logic for handling the map filters
                    //mapMaker.handleMapSwitch(agencyMap, $, i);
                }
                mapMaker.setUpClicks(agencyMap, popup, buildHtml, agencyMap.center, $, WP.wholeWorld.length);
                mapMaker.setUpMouseMoves(agencyMap, WP.wholeWorld.length, popup, buildHtml);
            });
        }
        
        if(WP.agencies) {
            var screenWidth = $(window).width();

            if (screenWidth > 425) {

                // Paul please put all this stuff into a seperate file at some point - PBird 16/02/16
                mapboxgl.accessToken = 'pk.eyJ1IjoiYmlyZHlib3kxOCIsImEiOiJjaWtoZGY0a3gwMDI3d2ptNGZ0ejNoNnRnIn0.RyfqOzDQmlDclZhHHjd38g';
                var regionDetails = mapMaker.getRegionDetails(WP.region);

                var agencyMap = new mapboxgl.Map({
                    container: 'agency-map',
                    //style: 'mapbox://styles/birdyboy18/cikzcg49k000z9klziviggy2s',
                    style: 'mapbox://styles/mapbox/light-v9',
                    center: regionDetails.center, // starting position
                    zoom: 2.5, // starting zoom
                    pitch: 0,
                    bearing: 0,
                    scrollZoom: false,
                });

                var popup = new mapboxgl.Popup({
                    closeButton: false,
                    anchor: 'right',
                    closeOnClick: false
                });

                agencyMap.on('load', function(){
                    
//                    $('#agency-map').addClass(regionDetails.class);
                    
                    agencyMap.addControl(new mapboxgl.NavigationControl(), 'top-left');

                    agencyMap.doubleClickZoom.enable();
                    
                    var source = 'agencyMarkers-' + WP.region;
                    agencyMap.addSource(source, {
                        'type':'geojson',
                        'data': WP.agencies
                    });
                    mapMaker.addAgencyMarkers(agencyMap, regionDetails, source, 0);
                    
                    source = 'recruitingMarkers-' + WP.region
                    agencyMap.addSource(source, {
                        'type':'geojson',
                        'data': WP.recruitingCountries
                    });
                    mapMaker.addRecruitingMarkers(agencyMap, 10, source, 0);
                    //this sets up the logic for handling the map filters
                    mapMaker.handleMapSwitch(agencyMap, $, 0);
                    mapMaker.setUpClicks(agencyMap, popup, buildHtml, regionDetails.center, $, 1);
                    mapMaker.setUpMouseMoves(agencyMap, 1, popup, buildHtml);
                });
            } else {
                var agencyListView = $('.agency-list-view');
                $('.view-switcher').addClass('hidden');
                agencyListView.removeClass('hidden');
            }
        }

        //if has any of the recruiting regions
        if (WP.memberRecruitingCountries) {
            var screenWidth = $(window).width();


            // Paul please put all this stuff into a seperate file at some point - PBird 16/02/16
            mapboxgl.accessToken = 'pk.eyJ1IjoiYmlyZHlib3kxOCIsImEiOiJjaWtoZGY0a3gwMDI3d2ptNGZ0ejNoNnRnIn0.RyfqOzDQmlDclZhHHjd38g';
            var regionDetails = mapMaker.getRegionDetails(WP.region);

            var agencyMap = new mapboxgl.Map({
                container: 'agency-map',
                //style: 'mapbox://styles/birdyboy18/cikzcg49k000z9klziviggy2s',
                style: 'mapbox://styles/mapbox/light-v9',
                //center: regionDetails.center, // starting position
                zoom: 2.5, // starting zoom
                pitch: 0,
                bearing: 0,
                scrollZoom: false,
            });

            var popup = new mapboxgl.Popup({
                closeButton: false,
                anchor: 'right',
                closeOnClick: false
            });

            agencyMap.on('load', function(){

                //$('#agency-map').addClass(regionDetails.class);

                agencyMap.addControl(new mapboxgl.NavigationControl({position: 'top-left'}));

                agencyMap.doubleClickZoom.enable();

                agencyMap.addSource('recruitingMarkers', {
                    'type':'geojson',
                    'data': WP.memberRecruitingCountries
                });

                memberMapMaker.addRecruitingMarkers(agencyMap, 10);
                //this sets up the logic for handling the map filters
                memberMapMaker.setUpClicks(agencyMap, popup, buildHtml, $);
                memberMapMaker.handleMapSwitch(agencyMap, $);
                memberMapMaker.setUpMouseMoves(agencyMap);
            });
        }
    } 
    
    function buildHtml(feature) {
        var html = '';
        if (!feature.properties.logo) {
            var agencyLogo = '<h3 class="uppercase bold white">' + feature.properties.title + '</h3>';
        } else {
            var agencyLogo = '<div class="agency-map-logo">' + feature.properties.logo + '</div>';
        }
        if (feature.properties.description) {
            var p = '<p>' + feature.properties.description + '</p>'
        } else if (feature.properties.markerType == 'recruiting') {
            var p = '<p>If you\'re an agency based in ' + feature.properties.title + ' that meets our criteria, then we want you.</p>'
        } else {
            var p = '<p></p>';
        }

//        if (!feature.properties.link) {
//            var link = '<a class="light inline-underline white" href="/members/?location=' + feature.properties.title + '#join-form">Apply now</a>';
//        } else {
//            var link = '<a class="light inline-underline white" href="' + feature.properties.link + '">Find out more</a>';
//        }

//        var closeIcon = '<div class="close-icon">×</div>';

//        html += closeIcon;
        html += agencyLogo;
        html += p;
//        html += link;
        return html;
    }

    function dynaHeight() {
        $el = $('.js-dyna-height')
        $el.css('height', 'auto');
        var height = $el.innerHeight();
        $el.attr('data-height', height);
        $el.css('height', '');
    }

    if($('.client-list').length > 0) {
        var clients = WP.clients;
        var client_filters = WP.client_filters;
        var clientModel = new clientModel();
        ko.applyBindings(clientModel);

        $('.view-clients').click(function() {
            $(this).toggleClass('isActive');
            $('.client-list').toggleClass('isActive');
        });
    }

    $(document).bind('gform_post_render', function(e,form,page){
        var $error = $('.validation_error');
        if ($error.length > 0) {
            $('.contact-wrapper').css('height', 'auto');
        }
    });

	//

	if($('.news-pods').length > 0) {
		console.log(WP);
        var newsModel = new newsModel();
        ko.applyBindings(newsModel);
        //get the hash so that we can replicate tag filtering
        newsModel.getHash();
    }

    var popupVideo = $('.js-popup-video');
    var popupVideoSrc = $('.js-popup-video').find('source').attr('data-src');
    var popupTrigger = $('.js-popup-video-trigger');
    var overlay = $('.popup-video-overlay');
	var popupClose = $('.video-overlay-close');

    popupTrigger.on('click', function(e){
        e.preventDefault();
        overlay.addClass('isActive');
		popupVideo.find('video source').attr('src', popupVideoSrc);
		popupVideo.find('video')[0].load();
		popupVideo.find('video')[0].play();
    });

	popupClose.on('click', function(e){
		overlay.removeClass('isActive');
		popupVideo.find('video')[0].pause();
	});

    // logic for switching tables on tab clicks
    // @todo get the height of the table and set the heigh of the table wraper to the current active table,
    // this way the table can have different sized heights.
    // hints: .height(), .innerHeight(), .css('property','value');
    var tabs = $('.js-schedule-table-tab');
    var tables = $('.js-schedule-table-wrapper table');
	var tableWrapper = $('.js-schedule-table-wrapper');

	//will set the height of the wrapper to the first table in the jquery table collection
	tableWrapper.css('height', tables.eq(0).innerHeight());

    tabs.on('click', function(e){
        e.preventDefault();
        var $this = $(this);

        var idx = $this.attr('data-index');

        $this.siblings().removeClass('active');
        $this.addClass('active');

        tables.each(function(i, el){
            var $el = $(el);
            var tableIdx = $el.attr('data-index');
            if (idx == tableIdx) {
                tableWrapper.css('height', $el.innerHeight());
                $el.addClass('active');
            } else {
                $el.removeClass('active');
            }
        });

    });

    $('.sponsors-slider').slick({
        infinite: true,
        dots: true,
        arrows: false,
        useTransform: true,
		autoplay: true,
  		autoplaySpeed: 2000
    });

    //Logic for showing and hiding the agency lists on the AMIN who is who page

    var $agencyLists = $('.js-agency-list-wrap');
    var $agencyListWrap = $('.js-who-is-who-agency-list');
    var $agencyBtn = $('.js-open-agency-list');

    //loop through each one and grab its height and store it the html for later usage
    $agencyLists.each(function(i, el) {
        var $el = $(el);

        $el.css('height', 'auto');
        var height = $el.innerHeight();
        $el.attr('data-height', height);
        $el.css('height', '');
    });

    $agencyBtn.on('click', function(e) {
        e.preventDefault();
        var $this = $(this);
        var region = $this.attr('data-region');

        $agencyLists.each(function(i, el) {
            var $el = $(el);
            var agencyListHeight = '';
            if ($el.attr('data-id') == region) {
                agencyListHeight = $el.attr('data-height');
                $agencyListWrap.css('height', agencyListHeight);
                $el.addClass('isActive');
            } else {
                $el.removeClass('isActive');
            }
            //scroll down to the agency list
            $('html, body').animate({
                //-80px to account for fixed header
                scrollTop: ($agencyListWrap.offset().top - 80)
            }, 500);
        });
    });

    //network seach results area logic

    function calculateNetworkResultsPos(extra) {
        var $search = $('.js-network-search input[type="text"]');
        var $searchResults = $('.js-network-search-results');
        //the top positions is the inputs offset + height - any fixed elements in this case the header and admin bar
        var searchBarPos = ($search.offset().top + $search.height()) - ($('.fixed-nav').height() + $('#wpadminbar').height()) + extra;

        $searchResults.css({
            'top': searchBarPos
        });
    }

    function calculateTallest($collection) {
        var tallest = 0;
        $collection.each(function(i, el) {
            var $el = $(el);

            if ($el.height() >= tallest) {
                tallest = $el.height();
            }
        });

        return tallest;
    }

    if ($('.js-network-search').length > 0) {
        var networkSearch = new networkSearchModel(WP.people, WP.agenciesData.agencies, WP.agenciesData.countries);
        ko.applyBindings(networkSearch);
        networkSearch.peopleResults.subscribe(function(vals) {
            networkSearch.isHovering(false);
        });
//        networkSearch.isHovering.subscribe(function(vals) {
//            var membersList = $('.members-list');
//            var membersImage = $('.members-image');
//
//            var listHeight = membersList.outerHeight();
//            var imageHeight = membersImage.outerHeight();
//
//            if (listHeight > imageHeight) {
//                membersImage.css({
//                    'height': listHeight
//                });
//            } else {
//                membersImage.css({
//                    'height': 'auto'
//                });
//            }
//        });
//        setTimeout(function() {
//            calculateNetworkResultsPos(30);
//        }, 300);
//        $(window).on('resize', function(){
//            calculateNetworkResultsPos(30);
//        });
    }

    //calculate the tallest team member and then set the height of all of them to be the tallest one
    if ($('.js-team-member').length > 0) {
        var $members = $('.js-team-member');
        $members.height(calculateTallest($members));
    }

	setTimeout(function() {
		if($(window).width() > 768) {
			$('.js-recruiting-modal').addClass('isActive');
		}
	}, 1000);

	$('.js-close-recruiting-modal').click(function() {
		$('.js-recruiting-modal').removeClass('isActive');
	});

	$('.js-benefit-video').click(function(e) {
		var videoSrc = $(this).data('src');
		$('.js-benefit-video-iframe').attr('src', videoSrc);
		$('.js-popup-video').parent().addClass('isActive');
	});
        
    $('.button-search').click(function() {
        $block = $('.button-search + div');
        if ($block.css('display') === "none") {
            $block.show('slow');
        }
        else {
            $block.hide('slow');
        }
    });

});

//
