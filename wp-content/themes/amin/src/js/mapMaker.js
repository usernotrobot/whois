/*
* This module expects a a mapbox map instance and will then get it to generate and add the layers as needed.
*/

var blue = '#126a9c';

module.exports = {
    addAgencyMarkers: function(map, markerColor, source, id) {
        map.addLayer({
            'id': "AgencyMarkersCircles-" + id,
            'type': 'circle',
            'interactive': true,
            'source': source,
            "paint": {
                'circle-radius': 15,
                'circle-color': '#dddedf'
            },
            'filter': ['==', 'markerType', 'agency']
        });

        map.addLayer({
            'id': "AgencyMarkersCirclesBorder-" + id,
            'type': 'circle',
            'interactive': true,
            'source': source,
            "paint": {
                'circle-radius': 12,
                'circle-color': markerColor.color
            }
        });

        map.addLayer({
            'id': "AgencyMarkersCirclesInner-" + id,
            'type': 'circle',
            'source': source,
            'interactive': true,
            "paint": {
                'circle-radius': 10,
                'circle-color': '#dddedf'
            }
        });

        map.addLayer({
            'id': 'AgencyMarkerText-' + id,
            'type': 'symbol',
            'source': source,
            'layout': {
                'text-field': 'A',
                'text-size': 14,
                'text-font': ['Clan Offc Pro Bold'],
                'text-allow-overlap': true
            },
            'paint': {
                'text-color': markerColor.color
            },
            'filter': ['==', 'markerType', 'agency']
        });

        //Active marker layers
        map.addLayer({
            'id': 'AgencyMarkersCirclesOuter-' + id,
            'type': 'circle',
            'interactive': true,
            'source': source,
            'paint': {
                'circle-color': markerColor.color,
                'circle-radius': 43,
                'circle-opacity': .3
            },
            'filter': ['==', 'title', '']
        });

        map.addLayer({
            'id': 'AgencyMarkersCirclesOuterInner-' + id,
            'type': 'circle',
            'source': source,
            'interactive': true,
            'paint': {
                'circle-color': markerColor.color,
                'circle-radius': 27,
                'circle-opacity': .5
            },
            'filter': ['==', 'title', '']
        });

        //Hover layers, only shown if you're hovering over the correct layer
        map.addLayer({
            'id': 'AgencyMarkersCirclesHover-' + id,
            'type': 'circle',
            'interactive': true,
            'source': source,
            'paint': {
                'circle-color': markerColor.color,
                'circle-radius': 15
            },
            'filter': ['==', 'title', '']
        });

        map.addLayer({
            'id': 'AgencyMarkerTextHover-' + id,
            'type': 'symbol',
            'source': source,
            'layout': {
                'text-field': 'A',
                'text-size': 14,
                'text-font': ['Clan Offc Pro Bold']
            },
            'paint': {
                'text-color': '#ffffff'
            },
            'filter': ['==', 'title', '']
        });

        //extra layers of active state
        map.addLayer({
            'id': 'AgencyMarkersCirclesActive-' + id,
            'type': 'circle',
            'interactive': true,
            'source': source,
            'paint': {
                'circle-color': markerColor.color,
                'circle-radius': 15
            },
            'filter': ['==', 'title', '']
        });

        map.addLayer({
            'id': 'AgencyMarkerTextActive-' + id,
            'type': 'symbol',
            'source': source,
            'layout': {
                'text-field': 'A',
                'text-size': 14,
                'text-font': ['Clan Offc Pro Bold']
            },
            'paint': {
                'text-color': '#ffffff'
            },
            'filter': ['==', 'title', '']
        });
    },

    addRecruitingMarkers: function(map, radius, source, id) {
        map.addLayer({
            'id': "RecruitingMarkersCircles-" + id,
            'type': 'circle',
            'interactive': true,
            'source': source,
            'paint': {
                'circle-radius': radius,
                'circle-color': '#444',
                'circle-opacity': .5
            },
            'filter': ['==', 'markerType', '']
        });

        //Active marker layers
        map.addLayer({
            'id': 'RecrutingMarkersCirclesOuter-' + id,
            'type': 'circle',
            'interactive': true,
            'source': source,
            'paint': {
                'circle-color': '#333',
                'circle-radius': (radius*3),
                'circle-opacity': .3
            },
            'filter': ['==', 'title', '']
        });

        map.addLayer({
            'id': 'RecruitingMarkersCirclesOuterInner-' + id,
            'type': 'circle',
            'source': source,
            'interactive': true,
            'paint': {
                'circle-color': '#333',
                'circle-radius': (radius*2),
                'circle-opacity': .5
            },
            'filter': ['==', 'title', '']
        });

        //Hover layers, only shown if you're hovering over the correct layer
        map.addLayer({
            'id': 'RecruitingMarkersCirclesHover-' + id,
            'type': 'circle',
            'interactive': true,
            'source': source,
            'paint': {
                'circle-color': '#333',
                'circle-radius': radius,
                'circle-opacity': .8
            },
            'filter': ['==', 'title', '']
        });


        //extra layers of active state
        map.addLayer({
            'id': 'RecruitingMarkersCirclesActive-' + id,
            'type': 'circle',
            'interactive': true,
            'source': source,
            'paint': {
                'circle-color': '#333',
                'circle-radius': radius
            },
            'filter': ['==', 'title', '']
        });
    },
    /*
    * Pass in the map you want to do some switching and jquery to do some dom selecting
    */
    handleMapSwitch: function(map, $, id) {
        var discoverBtnParent = $('.discover-map');
        var discoverBtn = $('.js-map-discover');
        var viewSwitcher = $('.view-switcher');

        discoverBtn.on('click', function(e){
            e.preventDefault();
            $this = $(this);
            if (!discoverBtnParent.hasClass('active')) {
                //reveal recruting markers
                map.setFilter('RecruitingMarkersCircles-' + id, ['==', 'markerType', 'recruiting']);

                //hide agency markers
                map.setFilter('AgencyMarkersCircles-' + id, ['==', 'markerType', '']);
                map.setFilter('AgencyMarkerText-' + id, ['==', 'markerType', '']);
                map.setFilter('AgencyMarkersCirclesBorder-' + id, ['==', 'markerType', '']);
                map.setFilter('AgencyMarkersCirclesInner-' + id, ['==', 'markerType', '']);

                discoverBtnParent.addClass('active');
                $this.addClass('active');
                $('.recruiting-text').removeClass('hidden');
                viewSwitcher.addClass('hidden');
                $('.agency-map').addClass('black');
            } else {
                //hide recruting markers
                map.setFilter('RecruitingMarkersCircles-' + id, ['==', 'markerType', '']);

                //reveal agency markers
                map.setFilter('AgencyMarkersCircles-' + id, ['==', 'markerType', 'agency']);
                map.setFilter('AgencyMarkerText-' + id, ['==', 'markerType', 'agency']);
                map.setFilter('AgencyMarkersCirclesBorder-' + id, ['==', 'markerType', 'agency']);
                map.setFilter('AgencyMarkersCirclesInner-' + id, ['==', 'markerType', 'agency']);

                $this.removeClass('active');
                discoverBtnParent.removeClass('active'); 
                $('.recruiting-text').addClass('hidden');
                viewSwitcher.removeClass('hidden');
                $('.agency-map').removeClass('black');
            }
        });
    },

    setUpClicks: function(map, popup, popupBuilder, regionCenter, $, length) {
        var thisMapMaker = this;
        map.on('click', function(e){
            layers = [];
            for (i = 0; i < length; i++ ){
                layers.push('AgencyMarkersCircles-' + i);
                layers.push('RecruitingMarkersCircles-' + i);
            }

            var features = map.queryRenderedFeatures(e.point, {
                layers: layers
            });

            if (!features.length > 0) {
                //we didn't find anything
            } else {
                var feature = features[0];

                var region = feature.layer.source;
                region = region.substr(region.indexOf('-')+1);
                
                var regionDetails = thisMapMaker.getRegionDetails(region);

                if (!popup.isOpen()) {
                    popup.setLngLat(feature.geometry.coordinates)
                    .setHTML(popupBuilder(feature))
                    .addTo(map);
                } else {
                    popup.setLngLat(feature.geometry.coordinates)
                    .setHTML(popupBuilder(feature));
                    $('.agency-map').removeClass('blue').removeClass('orange').removeClass('green');
                }
                $('.agency-map').addClass(regionDetails.class);

                //attach an event listener for the cross in popup
//                $('.mapboxgl-popup-content .close-icon, .js-zoom-out').on('click', function(e){
//                    module.exports.closeAndZoomOut(map, popup, regionCenter, length);
//                });
//                
//                if (map.getZoom() > 6) {
//                    var zoom = map.getZoom();
//                } else {
//                    var zoom = 6;
//                }
//
//                map.flyTo({
//                    center: feature.geometry.coordinates,
//                    zoom: zoom,
//                    pitch: 20
//                });
 
                if (!feature.properties.link) {
                    window.location.href = "/members/?location=" + feature.properties.title + "#join-form";
                } else {
                    window.location.href = feature.properties.link;
                }
                
//                for (i = 0; i < length; i++ ){
//                    map.setFilter('AgencyMarkersCirclesOuter-' + i, ['==', 'title', feature.properties.title]);
//                    map.setFilter('AgencyMarkersCirclesOuterInner-' + i, ['==', 'title', feature.properties.title]);
//                    map.setFilter('AgencyMarkersCirclesActive-' + i, ['==', 'title', feature.properties.title]);
//                    map.setFilter('AgencyMarkerTextActive-' + i, ['==', 'title', feature.properties.title]);
//
//                    map.setFilter('RecrutingMarkersCirclesOuter-' + i, ['==', 'title', feature.properties.title]);
//                    map.setFilter('RecruitingMarkersCirclesOuterInner-' + i, ['==', 'title', feature.properties.title]);
//                    map.setFilter('RecruitingMarkersCirclesActive-' + i, ['==', 'title', feature.properties.title]);
//                }
            }
        });
    },
    
    setUpMouseMoves: function(map, length, popup, popupBuilder) {
        var thisMapMaker = this;
        map.on('mousemove', function(e){

            layers = [];
            for (i = 0; i < length; i++ ){
                layers.push('AgencyMarkersCircles-' + i);
                layers.push('RecruitingMarkersCircles-' + i);
            }
            
            var features = map.queryRenderedFeatures(e.point, {
                //layers: ['AgencyMarkersCircles-1','RecruitingMarkersCircles-1', 'AgencyMarkersCircles-2','RecruitingMarkersCircles-2']
                layers: layers
            });
            
            map.getCanvas().style.cursor = features.length > 0 ? 'pointer' : '';

            if (!features.length > 0) {
                popup.remove();
                $('.agency-map').removeClass('blue').removeClass('orange').removeClass('green');
                
                for (i = 0; i < length; i++ ){
                    map.setFilter('AgencyMarkersCirclesHover-' + i, ['==', 'title', '']);
                    map.setFilter('AgencyMarkerTextHover-' + i, ['==', 'title', '']);
                    map.setFilter('RecruitingMarkersCirclesHover-' + i, ['==', 'title', '']);
                }
            } else {
                 var feature = features[0];

                if (feature) {
                    var region = feature.layer.source;
                    region = region.substr(region.indexOf('-')+1);

                    var regionDetails = thisMapMaker.getRegionDetails(region);

                    if (!popup.isOpen()) {
                        popup.setLngLat(feature.geometry.coordinates)
                        .setHTML(popupBuilder(feature))
                        .addTo(map);
                    } else {
                        popup.setLngLat(feature.geometry.coordinates)
                        .setHTML(popupBuilder(feature));
                        $('.agency-map').removeClass('blue').removeClass('orange').removeClass('green');
                    }
                    $('.agency-map').addClass(regionDetails.class);
                     
                    for (i = 0; i < length; i++ ){
                        map.setFilter('AgencyMarkersCirclesHover-' + i, ['==', 'title', feature.properties.title]);
                        map.setFilter('AgencyMarkerTextHover-' + i, ['==', 'title', feature.properties.title]);
                        map.setFilter('RecruitingMarkersCirclesHover-' + i, ['==', 'title', feature.properties.title]);
                    }
                    
                 } else {
                    for (i = 0; i < length; i++ ){
                        map.setFilter('AgencyMarkersCirclesHover-' + i, ['==', 'title', '']);
                        map.setFilter('AgencyMarkerTextHover-' + i, ['==', 'title', '']);
                        map.setFilter('RecruitingMarkersCirclesHover-' + i, ['==', 'title', '']);
                    }
                 }
            }
        });
    },

    closeAndZoomOut: function(map,popup, center, length) {
        popup.remove();
        $('.agency-map').removeClass('blue').removeClass('orange').removeClass('green');
        var arg = {
            zoom: 2.0,
            pitch: 0
        };
        if (center) {
            arg.center = center;
        };
        map.flyTo(arg);
        activeMarker = false;
        
        for (i = 0; i < length; i++ ){
            map.setFilter('AgencyMarkersCirclesOuter-' + i, ['==', 'title', '']);
            map.setFilter('AgencyMarkersCirclesOuterInner-' + i, ['==', 'title', '']);
            map.setFilter('AgencyMarkersCirclesActive-' + i, ['==', 'title', '']);
            map.setFilter('AgencyMarkerTextActive-' + i, ['==', 'title', '']);

            map.setFilter('RecrutingMarkersCirclesOuter-' + i, ['==', 'title', '']);
            map.setFilter('RecruitingMarkersCirclesOuterInner-' + i, ['==', 'title', '']);
            map.setFilter('RecruitingMarkersCirclesActive-' + i, ['==', 'title', '']);
        }
    },

    getRegionDetails: function(region) {
        var blue = '#126a9c';
        var orange = '#f38e4e';
        var green = '#00BC98';

        switch(region) {
            case 'americas':
                return {
                    'color': blue,
                    'class': 'blue',
                    'center': [-75.01387886338617, 16.72824651667497]
                };
                break;
            case 'europe-africa-the-middle-east':
                return {
                    'color': orange,
                    'class': 'orange',
                    'center': [29.62426207110748, 30.578755210404296]
                };
                break;
            case 'asia-pacific':
                return {
                    'color': green,
                    'class': 'green',
                    'center': [131.6554622859988, -12.9773465308959]
                };
                break;
            default:
                return {
                    'color': orange,
                    'class': 'orange',
                    'center': [29.62426207110748, 30.578755210404296]
                };
                break;
        }
    }
}
