<?php

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$args = array(
    'post_type' => 'person',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'meta_key' => 'agency_principle',
    'meta_value' => '1',
    'meta_compare' => '=',
    'orderby' => 'rand'
);


$context['leadership_team'] = Timber::get_posts($args);

foreach ($context['leadership_team'] as $k => $v) {

	$terms = $v->terms;
    $termStr = '';
    foreach($terms as $term) {
        $termStr = $termStr . ' ' . $term->slug;
    }
    $v->terms = $termStr;
}

$context['regions'] = Timber::get_terms('region', array('orderby' => 'count', 'order' => 'DESC'));

Timber::render('agency-principles.twig', $context);

?>
