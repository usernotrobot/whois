<?php
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$today = date('Ymd');

$context['page_hero'] = $post->get_field('page_hero');

$args = array(
	'post_type'	=> 'event',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
	'meta_query' => array(
		'relation' => 'OR',
		array(
			'key' => 'event_start_date',
			'compare' => '<',
			'value' => $today
		)
	)
);

$context['past_events'] = Timber::get_posts($args);

$args['meta_query'][0]['compare'] = '>';
$context['events'] = Timber::get_posts($args);

Timber::render(array('events.twig'), $context);
