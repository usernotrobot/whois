<?php
/*
Template Name: Landing Page
*/

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
if(WP_ENV == 'production'){
    $context['base_url'] = 'https://' . $_SERVER['SERVER_NAME'];
} else {
    $context['base_url'] = 'http://' . $_SERVER['SERVER_NAME'];
}

$context['post']->pageHero = $post->get_field('page_hero');
Timber::render(array($post->post_name . '.twig', 'page.twig'), $context);
