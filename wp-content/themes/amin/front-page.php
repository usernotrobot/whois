<?php

$context = Timber::get_context();
$context['page'] = Timber::get_post();

//Custom Fields
$context['sec_one_title'] = get_field('section_one_title');
$context['sliding_pod_title'] = get_field('sliding_pod_title');
$context['sliding_pod_copy'] = get_field('sliding_pod_copy');

$context['sec_two_title'] = get_field('section_two_title');
$context['member_bullets'] = get_field('member_bullets');
$context['client_bullets'] = get_field('client_bullets');

$agencies = count_users();
$context['agency_count'] = $agencies['avail_roles']['agency'];

$context['event'] = Timber::get_post($context['page']->event);

/* Original, leaving it here incase we need to revert back. - AFryer 20/02/2017
//Filters bit
$posts = Timber::get_posts(array(
	'post_type' => ['work', 'post'], // removed event for now.
	'posts_per_page' => 12,
	'order' => 'DESC',
	'orderby' => 'rand',
	'status' => 'published',
	'meta_query' => array(
		array(
			'key' => 'show_on_homepage',
			'value' => true
		),
	),
));

*/

//Filters bit
$postsWork = Timber::get_posts(array(
	'post_type' => ['work'], // removed event for now.
	'posts_per_page' => 6,
	'order' => 'DESC',
	'orderby' => 'date',
	'status' => 'published',
));

//Filters bit
$postsPost = Timber::get_posts(array(
	'post_type' => ['post'], // removed event for now.
	'posts_per_page' => 6,
	'order' => 'DESC',
	'orderby' => 'date',
	'status' => 'published',
));

$posts = array_merge($postsWork, $postsPost);

foreach ($posts as $k => $v) {
	$rand = rand(310, 450);
	$v->height = ceil( $rand / 20) * 20;
}

$context['postGrid'] = $posts;
foreach ($context['postGrid'] as $key => $value) {
	if($value->client){
		$client = get_post($value->client);
		$clientLogo = get_field('client_logo', $client->ID);
		$context['postGrid'][$key]->clientlogo = $clientLogo['url'];
	}
	$region = get_the_terms($value->ID, 'region');
	if($region){
		$region = $region[0]->slug;
		$context['postGrid'][$key]->region = $region;
	}
}

$context['filters'] = array(
	[
		'name' => 'Work',
		'slug' => 'work'
	],
	// [
	// 	'name' => 'Events',
	// 	'slug' => 'event'
	// ],
	[
		'name' => 'News',
		'slug' => 'post'
	]
);

Timber::render('home.twig', $context);
