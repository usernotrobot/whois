<?php
$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$args = array(
    'echo'         => 0,
    'post_type'    => 'page',
    'post_status'  => 'publish',
    'sort_column'  => 'menu_order',
    'title_li'     => NULL
);

$html = wp_list_pages($args);
$html = preg_replace('/( class="[^"]+")/is', '', $html);
$context['sitemap'] = '<ul>'. $html .'</ul>';

// $args = array(
//     'post_type' => 'client',
// 	'posts_per_page' => -1,
// 	'post_status' => 'publish'
// );
// $context['clientstest'] = get_posts($args);

Timber::render(array($post->post_name . '.twig', 'page.twig'), $context);
