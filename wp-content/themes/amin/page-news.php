<?php
//'/news' page. Couldn't use regular page because it conflicted with post type.
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['page_hero'] = $post->get_field('page_hero');

Timber::render(array('news.twig'), $context);
