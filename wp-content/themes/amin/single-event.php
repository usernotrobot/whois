<?php
/*
Template logic for displaying a single post.
*/

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$grabbedTags = get_the_tags();
if($grabbedTags){
	$context['tags'] = $grabbedTags;
}

$region = getRegion($post->ID);

if($region){
	$args = array(
		'post_type' => 'post',
		'order' => 'DESC',
		'orderby' => 'rand',
		'posts_per_page' => 4,
		'tax_query' => array(
	        array(
	            'taxonomy' => 'region',
	            'field'    => 'id',
	            'terms'    => $region->term_id,
	        ),
	    ),
	    'post__not_in' => array($post->ID)
	);
	query_posts($args);
	$context['relatedPosts'] = Timber::get_posts($args);
}

$today = date('Ymd');
$args = array(
	'post_type' => 'event',
	'posts_per_page' => -1,
	'meta_key' => 'event_start_date',
	'orderby' => 'meta_value_num',
	'order' => 'DESC',
	'meta_query' => array(
		array(
			'key' => 'event_start_date',
			'compare' => '>=',
			'value' => $today
		)
	)
);
query_posts($args);
$context['events'] = Timber::get_posts($args);

$args = array(
	'post_type' => 'post',
	'order' => 'DESC',
	'orderby' => 'date',
	'posts_per_page' => 3
);
query_posts($args);
$context['news'] = Timber::get_posts($args);

Timber::render('single-event.twig', $context);
