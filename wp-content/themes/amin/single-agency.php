<?php
/*
Template logic for displaying a single agency.
*/

$context = Timber::get_context();
$post = Timber::get_post();
$author = $post->author->ID;
$context['post'] = $post;
if(WP_ENV == 'production'){
	$context['base_url'] = 'https://' . $_SERVER['SERVER_NAME'];
} else {
	$context['base_url'] = 'http://' . $_SERVER['SERVER_NAME'];
}
$context['agencyid'] = $post->ID;
$context['workposts'] = Timber::get_posts(array(
	'post_type' => 'work',
	'order' => 'DESC',
	'orderby' => 'date',
	'posts_per_page' => -1,
	'author' => $author,
	'post_status' => 'publish'
));

foreach ($context['workposts'] as $key => $value) {
	if(get_field('listing_image', $value->ID)){
		$context['workposts'][$key]->image = get_field('listing_image', $value->ID);
	} else {
		$context['workposts'][$key]->image = get_field('banner_image', $value->ID);
	}
}

// This is now a handy getRegion function in utilities.php
$context['region'] = getRegion($post->ID);

// We are using this for the client SVG's until we as a team work out the right outcome.
$showcase = get_field('client_showcase');
if($showcase){
	foreach ($showcase as $key => $value) {
		$context['clients'][$key]['link'] = $value['url'];
		$context['clients'][$key]['logo'] = get_field('client_logo', $value['client']->ID);
		$context['clients'][$key]['logo']['url'] = str_replace($context['base_url'], 'https://s3.amazonaws.com/tj-aminworldwide', $context['clients'][$key]['logo']['url']);
	}
} else {
	$showcase = Timber::get_posts(array(
	    'post_type' => 'client',
	    'posts_per_page' => 12,
	    'author' => $author,
	    'post_status' => 'publish'
	));
	foreach ($showcase as $key => $value) {
		$context['clients'][$key]['logo'] = get_field('client_logo', $value->ID);
	}
}


$context['client_showcase'] = TRUE;

// if(get_field('client_showcase')) {
// 	$clients = array();
// 	foreach (get_field('client_showcase') as $k => $v) {
// 		if($v['client']) {
// 			$client = $v['client'];
// 			$v['client_logo'] = extractSVG(get_field('client_logo', $client->ID)['url']);
// 			//TW: Client needs a logo or we can't push anything here.
// 			array_push($clients, $v);
// 		}
// 	}
// 	$context['client_showcase'] = $clients;
// }

// if(get_field('logo', $post->ID)['url']) {
// 	$context['agency_logo'] = extractSVG(get_field('logo', $post->ID)['url']);
// }
$image = get_field('logo', $post->ID);
$context['agency_logo'] = str_replace($context['base_url'], 'https://s3.amazonaws.com/tj-aminworldwide', $image['url']);

$args = array(
	'post_type' => 'post',
	'order' => 'DESC',
	'posts_per_page' => 3,
	'author' => $author
);

$context['agency_news'] = Timber::get_posts($args);

//we only want to show team memebers to people who are logged in.
if ($context['is_admin']) {
    //get all of the people associated with an agency
    $args = array(
        'post_type' => 'person',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'meta_key' => 'agency',
        'meta_value' => $post->ID,
        'meta_compare' => '='
    );

    $staff = Timber::get_posts($args);

    $context['team_members'] = $staff;

    //get all of the associated assets added. from the assets repeater field

    $assets = $post->get_field('assets');

    $context['agency_assets'] = $assets;
}

Timber::render('single-agency.twig', $context);
