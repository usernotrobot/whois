<?php
$context = Timber::get_context();
$context['post'] = Timber::get_post();

$context['emea'] = Timber::get_post(246);
$context['americas'] = Timber::get_post(252);
$context['apac'] = Timber::get_post(249);

Timber::render(array('network.twig'), $context);
