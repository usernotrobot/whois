<?php
$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$args = array(
    'post_type' => 'person',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'meta_key' => 'leadership_team',
    'meta_value' => true,
    'orderby' => 'menu_order'
);

$context['leadership_team'] = Timber::get_posts($args);

foreach ($context['leadership_team'] as $k => $v) {

	$terms = $v->terms;
    $termStr = '';
    foreach($terms as $term) {
        $termStr = $termStr . ' ' . $term->slug;
    }
    $v->terms = $termStr;
}

$context['regions'] = Timber::get_terms('region', array('orderby' => 'count', 'order' => 'DESC'));

$context['post']->pageHero = $post->get_field('page_hero');


Timber::render(array($post->post_name . '.twig'), $context);
