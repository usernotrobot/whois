<?php


$context = Timber::get_context();
$post = Timber::get_post();
$author = get_the_author();
$context['post'] = $post;
$context['theworks'] = get_field('content_section', $post->ID);
$context['theworksnew'] = get_field('project_work_slides', $post->ID);

//get the agency profile linked to the suthor
$args = array(
    'post_type' => 'agency',
    'posts_per_page' => -1,
    'author' => $post->author->ID
);

// $authorAgency = Timber::get_posts($args)[0];

// $authorAgency->cover = $authorAgency->get_field('regional_background_image')['url'];
// $authorAgency->logo = $authorAgency->get_field('logo')['url'];

$context['collaborators'] = get_field('collaborators');
if($context['collaborators']){
	foreach ($context['collaborators'] as $key => $value) {
		$cover = get_field('regional_background_image', $value->ID);
		$logo = get_field('logo', $value->ID);
		if($cover['url']){
			$context['collaborators'][$key]->cover = $cover['url'];
		}
		if($logo['url']){
			$context['collaborators'][$key]->logo = $logo['url'];
		}
	}
    array_unshift($context['collaborators'], $authorAgency);
}
// else {
// 	$context['authoragency'] = $authorAgency;
// }

Timber::render('single-work-legacy.twig', $context);
