<?php

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

if($post->agency != "") {
	$agency = new TimberPost($post->agency);
	if(getRegion($agency->id)) {
		$agency->region = getRegion($agency->id)->name;
	}

	$context['agency'] = $agency;

	if(get_field('logo', $context['agency']->ID)['url']) {
		//$context['agency_logo'] = extractSVG(get_field('logo', $context['agency']->ID)['url']);
		$context['agency_logo'] = get_field('logo', $context['agency']->ID);
	}
}

Timber::render('single-person.twig', $context);
