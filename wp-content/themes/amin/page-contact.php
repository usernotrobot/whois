<?php
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['contactforms'] = get_field('forms');
foreach ($context['contactforms'] as $key => $value) {
    $context['forms'][$key]['title'] = $value['title'];
    $context['forms'][$key]['id'] = $value['id'];
    $context['forms'][$key]['shortcode'] = '[gravityform id=' . $value['id'] . ' title=false description=false ajax=true]';
}

Timber::render(array($post->post_name . '.twig'), $context);
