<?php

$context = Timber::get_context();
$context['main_text'] = get_field('404_page_text');
Timber::render('404.twig', $context);
