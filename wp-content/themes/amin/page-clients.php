<?php
$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$context['clientsArr'] = array_map(function($client) {
	return new TimberPost($client['client']->ID);
}, $post->get_field('selected_clients'));

Timber::render('clients.twig', $context);
