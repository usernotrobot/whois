<?php
	$context = Timber::get_context();
	$post = new TimberPost();
	$context['post'] = $post;

	$regions = array('americas', 'europe', 'asia');
	foreach ($regions as $key => $region) {
		$context[$region . '_contact'] = array(
			'name' => get_field($region . '_press_name'),
			'email' => get_field($region . '_press_email'),
			'address' => get_field($region . '_press_address'),
			'phone' => get_field($region .'_press_phone')
		);
	}

	$context['latest_news'] = Timber::get_posts(array(
		'post_type' => 'press-news',
		'post_status' => 'publish',
		'posts_per_page' => 3,
		'tax_query' => array(
			array(
				'taxonomy'  => 'press-news-type',
	            'field'     => 'slug',
	            'terms'     => 'awards',
	            'operator'  => 'NOT IN'
			)
		)
	));

	$context['awards'] = Timber::get_posts(array(
		'post_type' => 'press-news',
		'post_status' => 'publish',
		'posts_per_page' => 3,
		'tax_query' => array(
			array(
				'taxonomy'  => 'press-news-type',
	            'field'     => 'slug',
	            'terms'     => 'awards'
			)
		)
	));

	$context['speakers'] = Timber::get_posts(array(
		'post_type' => 'speaker',
		'post_status' => 'publish',
		'posts_per_page' => 4
	));

	//simply to check whether we have downloads
	$context['downloads'] = get_posts(array(
		'post_type' => 'press-download',
		'post_status' => 'publish'
	));

	$benefits = get_page_by_title('Benefits');
	$context['benefits_videos'] = get_field('videos', $benefits->ID);

	Timber::render('press-room.twig', $context);
?>
