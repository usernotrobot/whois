<?php
$context = Timber::get_context();
$context['page'] = Timber::get_post();


//Hide empty true doesn't work because of the approval system.
// $workTerms = get_terms('work-type', array(
//     'hide_empty' => true
// ));
//$context['workterms'] = $workTerms;

$workPosts = Timber::get_posts(array(
	'post_type' => array('work', 'legacy-work'),
	'order' => 'DESC',
	'orderby' => 'date',
	'posts_per_page' => -1,
	'post_status' => 'publish',
	// 'meta_query' => array(
	// 	array(
	// 		'key' => 'approved',
	// 		'value' => '1',
	// 	)
	// )
));

//print_r($workPosts);

//we only want to show the work terms if any of the posts here have them.
$workTerms = array();

foreach ($workPosts as $k => $v) {

    /*
    * we want to make a list of class terms base don the slug for filtering.
    * We then also make a list of all teh terms these posts have and push them into a
    * Variable outside this loop and then call array_unique to remove duplicates
    * This stops a list of work terms with no posts in it being grabbed.
    * Pbird - 24/03/16
    */
	$terms = $v->terms;
    $termStr = '';
    foreach($terms as $term) {
        $termStr = $termStr . ' ' . $term->slug;
        array_push($workTerms, array(
            'name' => $term->name,
            'slug' => $term->slug
        ));
    }
    $v->terms = $termStr;

	$rand = rand(310, 450);
	$v->height = ceil( $rand / 20) * 20;

	$client = get_field('client', $v->ID);
	if($client) {
		$v->clientlogo = get_field('client_logo', $client->ID);
	}
}
//go through our found work terms and then make them unique thus removing duplicates
$finalWorkTerms = array_unique($workTerms, SORT_REGULAR);

$context['workterms'] = $finalWorkTerms;

$context['workPosts'] = $workPosts;

Timber::render('work.twig', $context);
