<?php
/*
Template logic for displaying a single post.
*/

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$grabbedTags = get_the_tags();
if($grabbedTags){
	$context['tags'] = $grabbedTags;
}

$args = array(
	'post_type' => 'event',
	'post_status' => 'publish',
	'order' => 'DESC',
	'orderby' => 'date',
	'posts_per_page' => -1
);
query_posts($args);
$context['events'] = Timber::get_posts($args);

$args = array(
	'post_type' => 'press-news',
	'post_status' => 'publish',
	'order' => 'DESC',
	'orderby' => 'date',
	'post__not_in' => array($post->ID),
 	'posts_per_page' => 3
);

//if is award
if(has_term('awards', 'press-news-type', $post->ID)) {
	$additional_args = array(
		'tax_query' => array(
			array(
				'taxonomy' => 'press-news-type',
				'field' => 'slug',
				'terms' => array('awards')
			)
		)
	);
} else {
	$additional_args = array(
		'tax_query' => array(
			array(
				'taxonomy' => 'press-news-type',
				'field' => 'slug',
				'terms' => array('awards'),
				'operator' => 'NOT IN'
			)
		)
	);
}

$args = array_merge($args, $additional_args);

// query_posts($args);

$posts = Timber::get_posts($args);

$context['news'] = $posts;

Timber::render('single.twig', $context);
