<?php
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$context['page_hero'] = $post->>get_field('page_hero');

Timber::render(array('awards.twig'), $context);
