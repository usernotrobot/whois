<?php
/**
 * Custom Login page that will get loaded and re-directed too from wp-login.php
 */

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$loginArgs = array(
    'echo' => false,
    'redirect' => home_url(),
    'remember' => false,
    'id_username' => 'user',
    'id_password' => 'pass',
    'label_log_in' => __('Continue')
);

$context['login_html'] = wp_login_form($loginArgs);

if (isset($_GET['login'])) {
    $login = $_GET['login'];

    if ($login === 'false') {
        $context['login_status'] = array(
            'message' => 'Successfully Logged out',
            'status' => 'success'
        );
    } else if ($login === 'failed') {
        $context['login_status'] = array(
            'message' => 'There was an error, please make sure your username and password is correct.',
            'status' => 'error'
        );
    } else if ($login === 'empty') {
        $context['login_status'] = array(
            'message' => 'Please fill in both the username and password fields',
            'status' => 'error'
        );
    }
}

Timber::render('login.twig', $context);
