<?php

$context = Timber::get_context();

if (!$context['is_admin']) {
    wp_redirect('/login');
    exit;
}
$post = Timber::get_post();
$context['page'] = $post;

$context['emea'] = Timber::get_post(246);
$context['americas'] = Timber::get_post(252);
$context['apac'] = Timber::get_post(249);

//get all of the regions
$regions = Timber::get_terms('region');

// For quickness we will store stuff in a transient so that it's quicker to
// fetch and doesn't feel like it takes an age.

/**
 * We actually have transient-ed data like this before but it's not in a format
 * we want, so lets transient it, again.
 *
 */
$transient_key = '_who-is-who_agencies';
$transient = get_transient($transient_key);

if (empty($transient)) {
    $allAgencies = array();

    foreach($regions as $region) {
        $allAgencies[$region->name] = $region->posts(-1, 'agency');

        //loop through each one and get some extra needed acf data
        foreach ($allAgencies[$region->name] as $agency) {
			if(isset($agency->get_terms('country')[0])) {
				$agency->country = $agency->get_terms('country')[0];
			}
            $agency->coverImage = $agency->get_field('regional_background_image');
            // $agency->agencyLogo = extractSVG($agency->get_field('logo')['url']);
            $agency->agencyLogo = $agency->get_field('logo');
        };

        //sort them by country order or by title
        if ($region->slug != 'americas') {
            usort($allAgencies[$region->name], 'sortByCountry');
        } else {
            usort($allAgencies[$region->name], 'sortOnPostTitle');
        }
    };

    set_transient($transient_key, $allAgencies, HOUR_IN_SECONDS);
    $context['allAgencies'] = $allAgencies;
} else {
    $context['allAgencies'] = $transient;
}

Timber::render('who-is-who.twig', $context);
