<?php
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$args = array(
	'post_type' => 'speaker',
	'post_status' => 'publish',
	'posts_per_page' => -1
);

$context['speakers'] = Timber::get_posts($args);

Timber::render('speakers.twig', $context);
