<?php

$plugins = array(
	'/advanced-custom-fields-pro/acf.php',
	'/gravityforms/gravityforms.php',
	'/amazon-web-services/amazon-web-services.php',
	'/discourage-search-engines-by-url/discourage-search-engines-by-url.php',
	'/timber-library/timber.php',
	'/ssl-insecure-content-fixer/ssl-insecure-content-fixer.php'
);

foreach ($plugins as $key => $plugin) {
	$path = __DIR__ . $plugin;
	if(file_exists($path)) {
		require_once($path);
	}
}

?>
